var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('app.scss');
    mix.scripts(
        [
            'auth/services.js',
            'auth/controllers.js',
            'auth/directives.js',
            'auth/filters.js',
            'auth/module.js',


            'publicationsManager/services.js',
            'publicationsManager/controllers.js',
            'publicationsManager/directives.js',
            'publicationsManager/filters.js',
            'publicationsManager/module.js',

            'sitesManager/services.js',
            'sitesManager/controllers.js',
            'sitesManager/directives.js',
            'sitesManager/filters.js',
            'sitesManager/module.js',


            'supportManager/services.js',
            'supportManager/controllers.js',
            'supportManager/directives.js',
            'supportManager/filters.js',
            'supportManager/module.js',

            'usersManager/services.js',
            'usersManager/controllers.js',
            'usersManager/directives.js',
            'usersManager/filters.js',
            'usersManager/module.js',

            'app/controllers.js',
            'app/services.js',
            'app/directives.js',
            'app/filters.js',
            'app/module.js'
        ],
        'public/js/siteAdmApp.js');
});
