angular.module('auth.services', []);
angular.module('auth.services').provider('userHandler', function() {
    var user = {};

    return {
        load: function() {
            var localUser = JSON.parse(localStorage.getItem('user'));
            if (localUser) {
                user = localUser;
            }
        },
        getCurrentUser: function() {
            return user;
        },
        $get: function() {
            return {
                get: function() {
                    return user;
                },
                clear: function() {
                    user = {};
                }
            };
        }
    };


});
/*
* Модуль с контроллерами для авторизации
* */
angular.module('auth.controllers', [])
    .controller('AuthController', ['$scope', '$state', '$document', '$auth', '$rootScope', '$http', 'userHandler', function($scope, $state, $document, $auth, $rootScope, $http, userHandler) {
        /*
        * Основной контроллер, отвечающий за авторизацию
        * */
        $document[0].title = 'Вход';
        $scope.user = {};

        $rootScope.showAlertBlock = false;

        $rootScope.alertText = "";

        $scope.isRequestSend = false;

        $scope.authenticate = function() {
            $scope.isRequestSend = true;
            var credentials = {
                login: $scope.auth.login,
                password: $scope.auth.password
            };
            $auth.login(credentials).then(function() {
                return $http.get('/api/authenticate/user');
            }, function() {
                $scope.isRequestSend = false;
                $rootScope.showAlertBlock = true;
                $scope.user.password = "";
                $rootScope.alertText = "Неправильно указан логин или пароль.";
            }).then(function(response) {
                if (response) {
                    $scope.isRequestSend = false;
                    var user = JSON.stringify(response.data.user);
                    localStorage.setItem('user', user);
                    $rootScope.authenticated = true;
                    $rootScope.currentUser = response.data.user;
                    $state.go('publications');
                }
            });
        };

        $scope.closeAlert = function() {
            $rootScope.showAlertBlock = false;
        };


    }]);
angular.module('auth.directives', [])
    .directive('authAlert', function() {
        return {
            restrict: 'A',
            scope: {},
            controller: function(scope, element, attrs) {

            }
        };
    });
/*
* Модуль с фильтрами для авторизации
* */
angular.module('auth.filters', []);
/*
* Модуль отвечает за авторизацию пользователей
* */
angular.module('auth', ['auth.controllers', 'satellizer', 'auth.services', 'auth.directives'])
    .config(function($authProvider, $stateProvider, $urlRouterProvider) {
        $authProvider.loginUrl = '/api/authenticate';
    });
/*
* Модуль с сервисами для публикаций
* */
angular.module('publicationsManager.services', [])
    .factory('filesHandler', function() {

        var _validateFile = function(file) {

            var availableFileTypes = [
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.ms-excel',
                'image/png',
                'image/jpeg',
                'image/gif',
                'application/pdf'
            ];

            for (var i = 0; i < availableFileTypes.length; i++) {
                if (file.type === availableFileTypes[i]) {
                    return true;
                }
            }

            return false;
        };
        var _isMainFileSet = function(files) {
            for (var i = 0; i < files.length; i++) {
                if (files[i].isMain === true) {
                    return true;
                }
            }
            return false;
        };

        var _makeMimeTypeByExtension = function(fileName) {
            var extension = fileName.slice(fileName.lastIndexOf(".") + 1);
            var type = "";
            switch (extension) {
                case "doc":
                    type = "application/msword";
                    break;
                case "docx":
                    type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
            }
            return type;
        };

        var _drop = function(file) {
            return $http.post('/api/files?type=publications&action=drop&filePath=' + file.path, {});
        };



        return {
            validateFile: _validateFile,
            isMainFileSet: _isMainFileSet,
            makeMimeTypeByExtension: _makeMimeTypeByExtension,
            drop: _drop
        };


    })
    .factory('publicationsService', function($http) {

        var _create = function(publicationData) {
            return $http.post('/api/publications', publicationData);
        };

        var _delete = function(publicationId) {
            return $http.post('/api/publications/' + publicationId, {});
        };

        var _publish = function(publicationData) {
            return $http({
                method: 'PUT',
                url: '/api/publications/' + publicationData.id,
                data: publicationData
            });
        };

        return {
            create: _create,
            delete: _delete,
            publish: _publish
        };
    })
    .factory('termsService', function($http) {


        var _get = function(siteId, value) {
            return $http.get('/api/terms?siteId=' + siteId + '&search=' + value);
        };

        return {
            get: _get
        };

    });
/*
* Модуль с контроллерами для публикаций
* */
angular.module('publicationsManager.controllers', [])
    .controller('PublicationsListController', ['$scope', '$document', 'publicationsList', function($scope, $document, publicationsList) {
        /*
        * Контроллер для списка публикаций, разные отображения для разных ролей
        * */
        $document[0].title = 'Публикации';
        $scope.publications = publicationsList.data;



    }])
    .controller('PublicationsViewController', ['$scope', '$document', 'termsService', 'publicationsService', 'publicationData', function($scope, $document, termsService, publicationsService, publicationData) {
        /*
        * Контроллер для просмотра публикации
        * */
        $document[0].title = 'Просмотр публикации';



        $scope.publication = publicationData.data.publication;

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        var pubDateElement =  $('#pub-date');

        pubDateElement.datepicker({
            language: "ru"
        });


        $scope.showTerms = false;


        $scope.publication.pubDate = moment($scope.publication.pubDate, "YYYY-MM-DD").format('L');


        pubDateElement.on('blur', function(event) {

            if (!$scope.publication.pubDate || !event.target.value) {
                event.target.value = $scope.publication.pubDate;
            } else {
                $scope.publication.pubDate =  event.target.value;
            }


        });


        // для файлов
        $scope.publication.files = publicationData.data.files;


        for (var i = 1; i < publicationData.data.length - 1; i++) {
             $scope.publication.files.push({
                name: publicationData.data[i].name,
                path: publicationData.data[i].path,
                isMain: publicationData.data[i].isMain
             });
        }

        $scope.publication.site = {
            id: publicationData.data.sites[0].id,
            title: publicationData.data.sites[0].title
        };


        $scope.sendPublication = function() {

            if ($scope.pubForm.$valid) {

                publicationsService.publish($scope.publication).success(function() {

                    $.notify('Опубликовано.', {
                        globalPosition: "top center",
                        className: "success"
                    });

                    $location.path('admin/publications/posted');

                }).error(function() {


                });

            }


        };

        $scope.publication.receivedTerms = [];

        $scope.publication.chosenTerms = [];

        $scope.isSendTermsRequest = false;


        $scope.$watch('publication.termSearchItem', function(newValue) {


            if (typeof newValue == "string" && newValue.length > 2) {

                termsService.get($scope.publication.site.id, newValue).success(function(data) {

                    $scope.isSendTermsRequest = true;
                    $scope.publication.receivedTerms = [];

                    var chosenLength = $scope.publication.chosenTerms.length;

                    if (chosenLength == 0) {
                        $scope.publication.receivedTerms = data;
                    } else {

                        var receivedItemsLength = data.length;

                        // цикл по полученным элементам
                        for (var j = 0; j < receivedItemsLength; j++) {
                            var id = data[j].id;
                            var itemFound = false;
                            //цикл для проверки, есть ли такой элемент уже в выбранных
                            for (var i = 0; i < chosenLength; i++) {
                                if ($scope.publication.chosenTerms[i].id == id) {
                                    itemFound = true;
                                    break;
                                }
                            }
                            if (!itemFound) {
                                $scope.publication.receivedTerms.push(data[j]);
                            }
                            itemFound = false;
                        }
                    }

                }).error(function() {



                });


            } else {
                $scope.isSendTermsRequest = false;
                $scope.publication.receivedTerms = [];
            }

        });


        $scope.choseTerm = function(term) {
            $scope.publication.chosenTerms.push(term);
            var index = $scope.publication.receivedTerms.indexOf(term);
            $scope.publication.receivedTerms.splice(index, 1);
        };

        $scope.dropTerm = function(term) {
            $scope.publication.receivedTerms.push(term);
            var index = $scope.publication.chosenTerms.indexOf(term);
            $scope.publication.chosenTerms.splice(index, 1);
        };

        $scope.deletePublication = function(publicationId) {

            publicationsService.delete(publicationId).success(function() {

                $.notify('Публикация успешно удалена.', {
                    globalPosition: "top center",
                    className: "success"
                });

            }).error(function() {

                $.notify('Возникла ошибка при удалении. Повторите запрос позже.', {
                    globalPosition: "top center",
                    className: "error"
                });

            });

        };



    }])
    .controller('PublicationsCreateController', ['$scope', '$document', 'filesHandler', 'publicationsService', 'Upload', function($scope, $document, filesHandler, publicationsService, Upload) {
        /*
        * Контроллер для создания публикации
        * */
        $document[0].title = 'Создание новой публикации';


        $scope.publication = {};
        $scope.publication.files = [];

        var pubDateElement = $('#pub-date');

        pubDateElement.datepicker({
            language: "ru"
        });


        $scope.publication.pubDate = moment().format("L");

        $scope.showUploadBar = false;



        /*
         * Для календаря
         * */
        pubDateElement.on('blur', function(event) {

            if (!$scope.publication.pubDate || !event.target.value) {
                event.target.value = $scope.publication.pubDate;
            } else {
                $scope.publication.pubDate =  event.target.value;
            }

        });

        $scope.validateFile = function(file) {
            return filesHandler.validateFile(file);
        };

        $scope.validateMainFile = function(file) {
            return file.mimeType === 'application/msword' || file.mimeType === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        };

        $scope.dropFile = function(file) {

            filesHandler.drop(file).success(function() {
                console.log('Файл успешно удален!');
                var index = $scope.publication.files.indexOf(file);
                $scope.publication.files.splice(index, 1);

                if (file.isMain) {
                    $scope.publication.title = "";
                }

            }).error(function() {
                console.log('Ошибка при удалении файла!');
            });

        };

        $scope.changeMainFile = function(file) {

            for (var i = 0; i < $scope.publication.files.length; i++) {
                if ($scope.publication.files[i].isMain) {
                    $scope.publication.files[i].isMain = false;
                    break;
                }
            }

            var title = file.name;
            $scope.publication.title = title.substr(0, title.lastIndexOf('.'));
            file.isMain = true;

        };

        $scope.createPublication = function() {

            if ($scope.pubForm.$valid) {

                if ($scope.publication.files.length == 0) {
                    return;
                }

                /*if (!isMainFileSet($scope.publication.files)) {
                 return;
                 }*/

                publicationsService.create($scope.publication).success(function() {

                    console.log('Публикация успешно создана');

                }).error(function() {

                    console.log('Ошибка при создании публикации!');

                });

            }

        };

        $scope.upload = function(files) {

            if (files && files.length) {

                for (var i = 0; i < files.length; i++) {

                    var file = files[i];

                    Upload.upload({

                        url: '/api/files?type=publication&action=upload',
                        file: file

                    }).success(function(data, status, headers, config) {

                        config.file.path = data.path;
                        config.file.mimeType = config.file.type || filesHandler.makeMimeTypeByExtension(config.file.name);
                        config.file.realName = config.file.name;
                        config.file.isMain = false;
                        $scope.showUploadBar = false;
                        $scope.publication.files.push(config.file);

                        console.log($scope.publication.files);

                    }).xhr(function(xhr) {

                        xhr.addEventListener('loadstart', function(event) {

                            $scope.showUploadBar = true;

                        }, false);

                    });
                    $scope.showUploadBar = false;
                }
            }

        };



    }])
    .controller('PublicationsArchiveController', ['$scope', '$document', function($scope, $document) {
        /*
        * Контроллер для отображения списка архивных публикаций
        * */



    }]);
/*
* Модуль с директивами для публикаций
* */
angular.module('publicationsManager.directives', []);
/*
* Модуль с фильтрами для публикаций
* */
angular.module('publicationsManager.filters', [])
    .filter('parsePubStatus', function() {
        return function(rawStatus) {
            var status;
            switch (rawStatus) {
                case 'moderation':
                    status = 'В обработке';
                    break;
                case 'published':
                    status = 'Опубликовано';
                    break;
                default:
                    status = 'Unknown';
            }
            return status;
        };
    });
/*
* Модуль по управлению публикациями
* */
angular.module('publicationsManager', ['publicationsManager.controllers', 'publicationsManager.directives', 'publicationsManager.services', 'publicationsManager.filters'])
    .config(function($stateProvider) {

        /* Список всех публикаций */
        $stateProvider.state('publicationsList', {
            url: '/publications',
            templateUrl: '/templates/publicationsManager/publicationsList.html',
            resolve: {
                publicationsList: function($http) {
                    return $http.get('/api/publications');
                }
            },
            controller: 'PublicationsListController'
        });

        /* Просмотр одной публикации, при этом можно редактировать ее при определенных обстоятельствах */
        $stateProvider.state('publicationsView', {
            url: '/publications/{publicationId:int}',
            templateUrl: '/templates/publicationsManager/publicationsView.html',
            resolve: {
                publicationData: function($http, $stateParams) {
                    return $http.get('/api/publications/' + $stateParams.publicationId);
                }
            },
            controller: 'PublicationsViewController'
        });

        /* Создание новой публикации */
        $stateProvider.state('publicationsCreate', {
            url: '/publications/create',
            templateUrl: '/templates/publicationsManager/publicationsCreate.html',
            controller: 'PublicationsCreateController'
        });

        /* Архив с публикациями(список) */
        $stateProvider.state('publicationsArchive', {
            url: '/publications/archive',
            templateUrl: '/templates/publicationsManager/publicationsArchive.html',
            resolve: {},
            controller: 'PublicationsArchiveController'
        });
    });
/*
* Модуль с сервисами для менеджера сайтов
* */
angular.module('sitesManager.services', [])
    .factory('sitesHandler', function() {

        var site = {};

        return {
            get: function() {
                return site;
            },
            set: function(siteData) {
                site = siteData;
            }
        };



    })
    .factory('siteInfoService', function($http, $stateParams, sitesHandler) {


        return {
            update: function(info) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/info',
                    method: 'POST',
                    skipAuthorization: true,
                    data: info,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            }

        };
    })
    .factory('siteLayoutService', function(sitesHandler, $stateParams, $http) {
        return {
            setMenuItem: function(item) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/main_menu/' + item.id + '?is_active=' + item.is_active,
                    method: 'POST',
                    skipAuthorization: true,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            },
            updateLayoutHeader: function(layout) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/layout?section=header',
                    method: 'POST',
                    skipAuthorization: true,
                    data: layout,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            },
            updateLayoutFooter: function(layout) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/layout?section=footer',
                    method: 'POST',
                    skipAuthorization: true,
                    data: layout,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            },
            updateLayoutContainer: function(layout) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/layout?section=container',
                    method: 'POST',
                    skipAuthorization: true,
                    data: layout,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            }

        };
    })
    .factory('siteStyleService', function($http, sitesHandler) {

        return {
            save: function(style) {
                return $http({
                    url: sitesHandler.get().url + 'api/style',
                    method: 'POST',
                    skipAuthorization: true,
                    data: style.body,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                })
            }
        };

    })
    .factory('sitePublicationsService', function($http, sitesHandler, $stateParams) {

        var _update = function(publication) {
            return $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/publications/' + publication.id,
                method: 'POST',
                data: publication,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };
        var _delete = function(id) {
            return $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/publications/' + publication.id + '?delete=1',
                method: 'POST',
                data: {},
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };


        return {
            update: _update,
            delete: _delete
        };
    })
    .factory('siteScriptsService', function($http, sitesHandler) {
        var _update = function(data) {
            return $http({
                url: sitesHandler.get().url + 'api/scripts',
                method: 'POST',
                data: data,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };
        return {
            update: _update
        };
    })
    .factory('sitePagesService', function($http, sitesHandler, $stateParams) {

        var _create = function(pageData) {
            return $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/pages',
                method: 'POST',
                data: pageData,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };


        return {
            create: _create
        };

    })
    .factory('siteHeaderService', function($http, sitesHandler) {
        /* Сервис для работы с хедером сайта */
        var _save = function(data) {
            return $http({
                url: sitesHandler.get().url + 'api/header',
                method: 'POST',
                data: data,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };

        return {
            save: _save
        };

    })
    .factory('siteFooterService', function($http, sitesHandler) {
        /* Сервис для работы с футером сайта */
        var _save = function(data) {
            return $http({
                url: sitesHandler.get().url + 'api/footer',
                method: 'POST',
                data: data,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };

        return {
            save: _save
        };

    }).factory('siteCreateService', function($http) {
        var _create = function(site) {
            return $http.post('/api/sites', site);
        };

        return {
            create: _create
        };


    });
/*
* Модуль с контроллерами для менеджера сайтов
* */
angular.module('sitesManager.controllers', ['ui.codemirror'])
    .controller('SitesListController', ['$scope', '$document', 'sitesList', function($scope, $document, sitesList) {

        $scope.sites = sitesList.data.sites;
        console.log(sitesList);

        $document[0].title = 'Менеджер сайтов';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.uncheckAll = function() {

            for (var i = 0; i < $scope.sites.length; i++) {
                if (!$scope.sites[i].checked) {
                    continue;
                }
                $scope.sites[i].checked = false;
                $scope.checkedCounter--;
            }

        };

        $scope.save = function(content) {

            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    $http({
                        url: $scope.sites[i].url + 'api/additional_content?type=' + $scope.currentAction,
                        method: 'POST',
                        skipAuthorization: true,
                        data: content,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).success(function() {
                        $.notify('Сохранено', 'success');
                        console.log('Success!');
                    });
                }

            }

        };

        $scope.term = null;
        $scope.currentAction = null;
        $scope.showActionsArea = false;

        $scope.checkedCounter = 0;

        $scope.choseAll = function() {

            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    continue;
                }
                $scope.sites[i].checked = true;
                $scope.checkedCounter++;
            }


        };

        $scope.setChecked = function (site) {
            if (site.checked) {
                site.checked = false;
                $scope.checkedCounter--;
                if ($scope.checkedCounter == 0) {
                    $scope.term = null;
                    $scope.currentAction = null;
                }

            } else {
                site.checked = true;
                $scope.checkedCounter++;
            }

        };


        $scope.changeTermTitle = function(term) {
            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    $http({
                        url: $scope.sites[i].url + 'api/terms',
                        method: 'POST',
                        skipAuthorization: true,
                        data: term,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).success(function() {
                        $.notify('Сохранено', 'success');
                        console.log('Success!');
                    });
                }

            }
        };

        $scope.changeAdditionalContent = function(content) {

            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    $http({
                        url: $scope.sites[i].url + 'api/additional_content?type=' + $scope.currentAction,
                        method: 'POST',
                        skipAuthorization: true,
                        data: content,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).success(function() {
                        $.notify('Сохранено', 'success');
                    }).error(function() {
                        $.notify('Возникла ошибка!', 'error');
                    });
                }

            }
        };


    }])
    .controller('SitesCreateController', ['$scope', '$document', 'siteCreateService', function($scope, $document, siteCreateService) {
        /* Контроллер для создания сайта */
        $document[0].title = 'Создание сайта';

        $scope.createSite = function(site) {
            siteCreateService.create(site).success(function() {
                $.notify('Сайт успешно создан!', 'success');
            }).error(function() {
                $.notify('Ошибка при создании сайта!', 'error');
            });
        };
    }])
    .controller('SitesInfoController', ['$scope', 'info', '$document', 'siteInfoService', '$rootScope', 'sitesHandler', '$stateParams', function($scope, info, $document, siteInfoService, $rootScope, sitesHandler, $stateParams) {
        /* Контроллер для отображения и заполнения основной информации о поселении */
        $document[0].title = 'Менеджер сайтов';

        $scope.siteInfo  = info.data;

        $scope.save = function(siteInfo) {
            siteInfoService.update(siteInfo).success(function() {
                $.notify('Сохранено!', 'success');
            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };


    }])
    .controller('SitesStylesController', ['$scope', '$document', 'style', 'siteStyleService', function($scope, $document, style, siteStyleService) {
        /* Контроллер для управления стилями сайта */
        $document[0].title = 'Стили сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.style = {};

        $scope.style.body = style.data;

        $scope.save = function(style) {
            siteStyleService.save(style).success(function() {

                $.notify('Сохранено!', 'success');

            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };

    }])
    .controller('SitesPagesController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для отображения страниц сайта */
        $document[0].title = 'Страницы сайта';
        $scope.pages = [];

        /*$scope.siteId = $stateParams.siteId;
        $scope.pages = pages.data;

        $scope.formData = {};

        $scope.showForm = false;
        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };


        $scope.showPage = function(id) {
            $location.path('admin/sites/' + $scope.siteId + '/pages/' + id);
        };

        $scope.showPageForm = function() {
            $scope.showForm = true;
        };

        $scope.cancelForm = function() {
            $scope.showForm = false;
            $scope.formData = null;
        };
*/
    }])
    .controller('SitesLayoutController', ['$scope', 'layout', '$document', 'menu', 'siteLayoutService', function($scope, layout, $document, menu, siteLayoutService) {

        $document[0].title = 'Разметка сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.layout = {};


        $scope.layout.header = layout.data.header;
        $scope.layout.footer = layout.data.footer;
        $scope.layout.container = layout.data.container;

        $scope.menu = {};
        $scope.menu.items = menu.data.menu;

        $scope.saveLayoutHeader = function(layout) {
            siteLayoutService.updateLayoutHeader(layout).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

        $scope.saveLayoutFooter = function(layout) {
            siteLayoutService.updateLayoutFooter(layout).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

        $scope.saveLayoutContainer = function(layout) {
            siteLayoutService.updateLayoutContainer(layout).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

        $scope.setActiveItem = function(item) {
            item.is_active = true;
            siteLayoutService.setMenuItem(item).success(function() {
                $.notify('Сохранено!', 'success');
            });

        };


        $scope.setNonActiveItem = function(item) {
            item.is_active = false;
            siteLayoutService.setMenuItem(item).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

    }])
    .controller('SitesPagesViewController', ['$scope', 'page', '$http', 'sitesHandler', '$stateParams', '$document', function($scope, page, $http, sitesHandler, $stateParams, $document) {

        $scope.content = page.data;
        $scope.siteId = $stateParams.siteId;

        $document[0].title = "Страницы сайта";

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };


        $scope.save = function(content) {
            $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/pages/' + $stateParams.remoteId,
                method: 'POST',
                skipAuthorization: true,
                data: content,
                headers: {
                    'Content-Type': 'text/plain'
                }
            }).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

    }])
    .controller('SitesMainPageController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для управления основной страницей сайта и виджетами */
        $document[0].title = 'Основная страница сайта';



    }])
    .controller('SitesPublicationsController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для отображения публикаций с сайта */
        $document[0].title = 'Список публикаций';
        $scope.publications = [];



        /*$scope.showPublication = function(id) {
            $location.path('/admin/sites/' + $stateParams.siteId + '/publications/' + id);
        };*/



    }])



    .controller('SitesTermsController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для управления рубриками */
        $document[0].title = 'Рубрики сайта';

        //$scope.terms = terms.data;

        $scope.terms = [];



    }])
    .controller('SitesPublicationsViewController', ['$scope', 'publication', 'sitePublicationsService', function($scope, publication, sitePublicationsService) {

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.publication = publication.data;

        $scope.savePublication = function(publication) {
            sitePublicationsService.update(publication).success(function(data) {
                $.notify('Сохранено!', 'success');
            });
        };

    }])

    .controller('SitesScriptsController', ['$scope', '$document', 'scripts', 'siteScriptsService', function($scope, $document, scripts, siteScriptsService) {
        /* Контроллер для управления скриптами сайта */
        $document[0].title = 'Скрипты сайта';

        $scope.scripts = scripts.data;

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.save = function(data) {
            siteScriptsService.update(data).success(function() {

                $.notify('Сохранено!', 'success');

            }).error(function() {

                $.notify('Возникла ошибка при сохранении!', 'error');

            });
        };

    }])
    .controller('SitesPagesCreateController', ['$scope', 'sitePagesService', '$location', '$stateParams', function($scope, sitePagesService, $location, $stateParams) {

        $scope.formData = {};

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.siteId = $stateParams.siteId;

        $scope.createPage = function(pageData) {
            sitePagesService.create(pageData).success(function(data) {
                $.notify('Страница успешно создана!', 'success');
                $location.path('sites/' + $stateParams.siteId + '/pages');
            }).error(function(data) {
                $.notify('Произошла ошибка', 'error');
            });
        };
    }])

    .controller('SitesMenuController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для управления меню сайта */
        $document[0].title = 'Редактирование основного меню сайта';




    }])
    .controller('SitesHeaderController', ['$scope', '$document', 'header', 'siteHeaderService', function($scope, $document, header, siteHeaderService) {
        /* Контроллер для управления хедером сайта */
        $document[0].title = 'Редактирование хедера сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };
        $scope.header = {};
        $scope.header.body = header.data;

        $scope.save = function(data) {
            siteHeaderService.save(data).success(function() {
                $.notify('Сохранено!', 'success');
            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };



    }])
    .controller('SitesFooterController', ['$scope', '$document', 'footer', 'siteFooterService', function($scope, $document, footer, siteFooterService) {
        /* Контроллер для управления футером сайта */
        $document[0].title = 'Редактирование футера сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.footer = {};
        $scope.footer.body = footer.data;



        $scope.save = function(data) {
            siteFooterService.save(data).success(function() {
                $.notify('Сохранено!', 'success');
            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };


    }]);
/*
* Модуль с директивами менеджера сайтов
* */
angular.module('sitesManager.directives', [])
    .directive('vmWidgetText', function() {
        return {
            restrict: 'EA',
            scope: {
                title: '=vmTitle',
                class: '=vmClass',
                content: '=vmContent',
                save: '&vmSave'
            },
            templateUrl: '/views/widgets/textWidget.html',
            link: function(scope, elem, attrs) {


            }
        };
    })
    .directive('vmWidgetBanners', function() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/views/widgets/bannersWidget.html',
            controller: function($scope) {

            },
            link: function(scope, elem, attrs) {

            }
        };
    })
    .directive('vmTermsTree', function() {
        return {

            restrict: 'A',

            scope: {
                terms: '=vmTreeData'
            },

            link: function(scope, elem, attrs) {

                var data = scope.terms;

                var mainUl = angular.element('<ul>'); // корень
                var mainLi = angular.element('<li>');

                for (var i = 0; i < data.children.length; i++) {
                    // по детям
                    var ul = angular.element('<ul>');
                    var elementChildren = data.children[i];

                    for (var j = 0; j < elementChildren.length; j++) {
                            var li
                    }


                }



            }

        };
    });
/*
* Модуль с фильтрами для менеджера сайтов
* */
angular.module('sitesManager.filters', []);
/*
* Модуль для управления сайтами
* */
angular.module('sitesManager', ['sitesManager.controllers', 'sitesManager.directives', 'sitesManager.services', 'sitesManager.filters'])
    .config(function($stateProvider) {

        /* Список всех сайтов */
        $stateProvider.state('sitesList', {
            url: '/sites',
            templateUrl: '/templates/sitesManager/sitesList.html',
            resolve: {
                sitesList: function($http) {
                    return $http.get('/api/sites');
                }
            },
            controller: 'SitesListController'
        });

        /* Базовый шаблон для просмотра сайта, включает себя меню вспомогательное */
        $stateProvider.state('sitesView', {
            url: '/sites/{siteId:int}',
            templateUrl: '/templates/sitesManager/sitesView.html'
        });

        /* Страница с просмотром и редактированием основной информации о поселении */
        $stateProvider.state('sitesView.info', {
            url: '/info',
            templateUrl: '/templates/sitesManager/sitesViewInfo.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                info: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/info', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesInfoController'
        });

        /* Главная страница сайта */
        $stateProvider.state('sitesView.mainPage', {
            url: '/main',
            templateUrl: '/templates/sitesManager/sitesViewMainPage.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                widgets: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/widgets', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesMainPageController'
        });

        /* Публикации сайта */
        $stateProvider.state('sitesView.publications', {
            url: '/publications',
            templateUrl: '/templates/sitesManager/sitesViewPublications.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                publications: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/publications', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPublicationsController'
        });


        /* Просмотр и редактирование одной публикации */
        $stateProvider.state('sitesView.publicationsView', {
            url: '/publications/{publicationId:int}',
            templateUrl: '/templates/sitesManager/sitesViewPublicationsView.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                publication: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/publications/' + $stateParams.publicationId, {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPublicationsViewController'
        });


        /* Просмотр и изменение стилей сайта */
        $stateProvider.state('sitesView.styles', {
            url: '/styles',
            templateUrl: '/templates/sitesManager/sitesViewStyles.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                style: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/style', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesStylesController'
        });


        /* Просмотр и изменение разметки сайта */
        $stateProvider.state('sitesView.layout', {
            url: '/layout',
            templateUrl: '/templates/sitesManager/sitesViewLayout.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                layout: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/layout', {
                        skipAuthorization: true
                    });
                },
                menu: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/main_menu', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesLayoutController'
        });

        /* Просмотр страниц сайта */
        $stateProvider.state('sitesView.pages', {
            url: '/pages',
            templateUrl: '/templates/sitesManager/sitesViewPages.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                pages: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/pages', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPagesController'
        });

        /* Просмотр одной страницы сайта */
        $stateProvider.state('sitesView.pagesView', {
            url: '/pages/{remoteId:int}',
            templateUrl: '/templates/sitesManager/sitesViewPagesView.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                page: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/pages/' + $stateParams.remoteId, {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPagesViewController'
        });


        /* Создание новой страницы сайта */
        $stateProvider.state('sitesView.pagesCreate', {
            url: '/pages/create',
            templateUrl: '/templates/sitesManager/sitesViewPagesCreate.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                terms: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/terms', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPagesCreateController'
        });

        /* Просмотр рубрик сайта */
        $stateProvider.state('sitesView.terms', {
            url: '/terms',
            templateUrl: '/templates/sitesManager/sitesViewTerms.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                terms: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/terms', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesTermsController'
        });


        /* Просмотр и редактирование скриптов сайта */
        $stateProvider.state('sitesView.scripts', {
            url: '/scripts',
            templateUrl: '/templates/sitesManager/sitesViewScripts.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                scripts: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);
                    return $http.get(siteUrl.data.siteURL.url + 'api/scripts', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesScriptsController'
        });




        /* Просмотр и редактирование футера сайта */
        $stateProvider.state('sitesView.footer', {
            url: '/footer',
            templateUrl: '/templates/sitesManager/sitesViewFooter.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                footer: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/footer', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesFooterController'
        });

        /* Просмотр и редактирование хедера сайта */
        $stateProvider.state('sitesView.header', {
            url: '/header',
            templateUrl: '/templates/sitesManager/sitesViewHeader.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                header: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/header', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesHeaderController'
        });


        /* Редактирование основного меню сайта */
        $stateProvider.state('sitesView.menu', {
            url: '/menu',
            templateUrl: '/templates/sitesManager/sitesViewMenu.html',
            resolve: {},
            controller: 'SitesMenuController'
        });





        /* Форма для созания нового сайта */
        $stateProvider.state('sitesCreate', {
            url: '/sites/create',
            templateUrl: '/templates/sitesManager/sitesCreate.html',
            controller: 'SitesCreateController'
        });

    })
    /*.run(function($rootScope, sitesHandler, $stateParams) {


        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {

            if (fromState.name === 'baseAdmin.sitesManager') {
                var sites = sitesHandler.getAll();

                sites.forEach(function(item) {
                    if (item.checked) {
                        delete item.checked;
                    }
                });


            }

        });

    })*/;

/*
* Модуль с сервисами для тех. поддержки
* */
angular.module('supportManager.services', [])
    .factory('supportService', function($http) {
        /* Сервис для управления заявками */
        var _saveColor = function(item, color) {
            switch(color) {
                case 'blue':
                    color = 'primary';
                    break;
                case 'orange':
                    color = 'secondary';
                    break;
                case 'default':
                    color = 'default';
                    break;
            }
            var data = {
                color: color
            };

            return $http({
                method: 'PUT',
                url: '/api/support/color/' + item.id,
                data: data
            })

        };

        var _restoreColors = function(item, color) {
            switch(color) {
                case 'primary':
                    item.primaryColor = true;
                    item.secondaryColor = false;
                    break;
                case 'secondary':
                    item.secondaryColor = true;
                    item.primaryColor = false;
                    break;
                case 'default':
                    item.secondaryColor = false;
                    item.primaryColor = false;
                    break;
            }
        };


        var _check = function(supportId) {
            return $http({
                url: '/api/support/' + supportId,
                data: {},
                method: 'PUT'
            });
        };

        var _create = function(supportData) {
            return $http.post('/api/support', supportData);
        };


        return {
            saveColor: _saveColor,
            restoreColors: _restoreColors,
            check: _check,
            create: _create
        };
    })
    .factory('supportFilesService', function($http) {
        /* Сервис по работе с файлами заявок */


        var _drop = function(file) {
            return $http.post('/api/files?type=support&action=drop&filePath=' + file.path, {});
        };

        return {
            drop: _drop

        };



    });
/*
* Модуль с контроллерами для тех. поддержки
* */
angular.module('supportManager.controllers', [])
    .controller('SupportListController', ['$scope', '$document', 'supportService', 'supportList', '$http', function($scope, $document, supportService, supportList, $http) {
        /*
        * Контроллер для отображения и взаимодействия с заявками в тех. поддержку
        * supportItems - список(массив) заявок в тех. поддержку
        * */
        $document[0].title = 'Тех. поддержка';
        $scope.supportItems = supportList.data;


        /* Восстановление цвета заявок при смене стейтов(к примеру) */
        $scope.supportItems.forEach(function(item) {
            switch(item.color) {
                case 'primary':
                    item.primaryColor = true;
                    item.secondaryColor = false;
                    break;
                case 'secondary':
                    item.secondaryColor = true;
                    item.primaryColor = false;
                    break;
                case 'default':
                    item.secondaryColor = false;
                    item.primaryColor = false;
                    break;
            }
        });


        $scope.changeColor = function(item, color) {

            switch(color) {
                case 'blue':
                    color = 'primary';
                    break;
                case 'orange':
                    color = 'secondary';
                    break;
                case 'default':
                    color = 'default';
                    break;
            }


            var data = {
                color: color
            };

            $http({
                method: 'PUT',
                url: '/api/support/color/' + item.id,
                data: data
            }).success(function(data) {
                //присвоить класс элементу
                console.log(data);


                switch(color) {
                    case 'primary':
                        item.primaryColor = true;
                        item.secondaryColor = false;
                        break;
                    case 'secondary':
                        item.secondaryColor = true;
                        item.primaryColor = false;
                        break;
                    case 'default':
                        item.secondaryColor = false;
                        item.primaryColor = false;
                        break;
                }

            });
        };


    }])
    .controller('SupportViewController', ['$scope', '$document', 'supportService', '$stateParams', 'supportData', function($scope, $document, supportService, $stateParams, supportData) {
        /*
        * Контроллер для просмотра заявки
        * */
        $document[0].title = 'Просмотр заявки';

        $scope.support = supportData.data.supportData;
        $scope.support.files = supportData.data.supportFiles;
        var supportId = $stateParams.supportId;





        $scope.updateSupportItem = function() {

            supportService.check(supportId)
                .success(function() {
                    console.log('Заявка успешно обработана!');

                    $.notify('Заявка успешно обработана.', {
                        globalPosition: "top center",
                        className: "success"
                    });
                })
                .error(function() {
                    console.log('Ошибка при обработке заявки!');
                });
        };


    }])
    .controller('SupportCreateController', ['$scope', '$document', 'supportService', 'supportFilesService', 'Upload', function($scope, $document, supportService, supportFilesService, Upload) {
        /*
        * Контроллер для создания новой заявки
        * */
        $document[0].title = 'Создание заявки в тех. поддержку';

        $scope.support = {};
        $scope.support.files = [];

        $scope.sendSupportMessage = function() {

            if ($scope.supForm.$invalid) {
                return;
            }
            supportService.create($scope.support);
        };

        $scope.dropFile = function(file) {

            supportFilesService.drop(file)
                .success(function() {
                    console.log('Файл успешно удаален!');

                    var index = $scope.support.files.indexOf(file);
                    $scope.support.files.splice(index, 1);

                })
                .error(function() {
                    console.log('Ошибка при удалении файла!');
                });

        };

        $scope.upload = function(files) {

            if (files && files.length) {

                for (var i = 0; i < files.length; i++) {

                    var file = files[i];

                    Upload
                        .upload({
                            url: '/api/files?type=support&action=upload',
                            file: file
                        })
                        .success(function(data, status, headers, config) {
                            config.file.path = data.path;
                            config.file.realName = config.file.name;
                            $scope.showUploadBar = false;
                            $scope.support.files.push(config.file);
                        })
                        .error(function() {
                            console.log('Ошибка при загрузке файла!');
                        })
                        .xhr(function(xhr) {
                            xhr.addEventListener('loadstart', function(event) {
                                $scope.showUploadBar = true;
                            }, false);
                        });

                }
                $scope.showUploadBar = false;

            }

        };


    }]);

/*
* Модуль с директивами для тех. поддержки
* */
angular.module('supportManager.directives', []);
/*
* Модуль с фильтрами для тех. поддержки
* */
angular.module('supportManager.filters', [])
    .filter('parseSupportStatus', function() {
        return function(rawStatus) {
            var status;

            switch (rawStatus) {
                case 'processed':
                    status = 'Обработано';
                    break;
                case 'send':
                    status = 'В обработке';
                    break;
            }

            return status;


        };
    });
/*
* Модуль для работы с заявками в тех. поддержку
* */
angular.module('supportManager', ['supportManager.controllers', 'supportManager.services', 'supportManager.directives', 'supportManager.filters'])
    .config(function($stateProvider) {

        /* Список заявок */
        $stateProvider.state('supportList', {
            url: '/support',
            templateUrl: '/templates/supportManager/supportList.html',
            resolve: {
                supportList: function($http) {
                    return $http.get('/api/support');
                }
            },
            controller: 'SupportListController'
        });

        /* Просмотр одной заявки */
        $stateProvider.state('supportView', {
            url: '/support/{supportId:[0-9]{1,}}',
            templateUrl: '/templates/supportManager/supportView.html',
            resolve: {
                supportData: function($http, $stateParams) {
                    return $http.get('/api/support/' + $stateParams.supportId);
                }
            },
            controller: 'SupportViewController'
        });


        /* Создание заявки */
        $stateProvider.state('supportCreate', {
            url: '/support/create',
            templateUrl: '/templates/supportManager/supportCreate.html',
            controller: 'SupportCreateController'
        });


    });
/*
* Модуль с сервисами для пользователей
* */
angular.module('usersManager.services', [])
    .factory('usersService', ['$http', function($http) {
        /*
        * Сервис для управления пользователями(CRUD)
        * */
        var _create = function(user) {
            return $http.post('/api/users', user);
        };

        return {
            create: _create
        };

    }]);
/*
* Модуль с контроллерами для пользователей
* */
angular.module('usersManager.controllers', [])
    .controller('UsersListController', ['$scope', '$document', 'usersList', function($scope, $document, usersList) {
        /*
        * Контроллер для списка пользователей
        * usersData - массив с пользователями
        * */
        $document[0].title = 'Пользователи';
        $scope.users = usersList.data;



    }])
    .controller('UsersCreateController', ['$scope', '$document', 'usersService', function($scope, $document, usersService) {
        /*
        * Контроллер для создания нового пользователя
        * */
        $document[0].title = 'Создание нового пользователя';

        $scope.createUser = function(user) {
            usersService.create(user)
                .success(function() {
                    console.log('Пользователь успешно создан!');
                })
                .error(function() {
                    console.log('Ошибка при создании пользователя!');
                });
        };

    }])
    .controller('UsersViewController', ['$scope', '$document', function($scope, $document) {
        /*
        * Контроллер для просмотра информации о пользователе и редактировния
        * */
        $document[0].title = 'Просмотр профиля пользователя';




    }]);
/*
* Модуль с директивами для пользователей
* */
angular.module('usersManager.directives', []);
/*
* Модуль с фильтрами для пользователей
* */
angular.module('usersManager.filters', [])
    .filter('parseRole', function() {
        /*
        * Фильтр для human-readable отображения названия ролей пользователей
        * */
        return function(rawRole) {
            var role;
            switch (rawRole) {
                case 'user':
                    role = 'Пользователь';
                    break;
                case 'moderator':
                    role = 'Модератор';
                    break;
                case 'user-moderator':
                    role = 'Пользователь-модератор';
                    break;
                default:
                    role = 'Unknown';
            }
            return role;
        };
    });
/*
* Модуль по управлению ппользователями
* */
angular.module('usersManager', ['usersManager.controllers', 'usersManager.directives', 'usersManager.services', 'usersManager.filters'])
    .config(function($stateProvider) {
        /* Список пользователей */
        $stateProvider.state('usersList', {
            url: '/users',
            templateUrl: '/templates/usersManager/usersList.html',
            resolve: {
                usersList: function($http) {
                    return $http.get('/api/users');
                }
            },
            controller: 'UsersListController'
        });

        /* Форма создания пользователя */
        $stateProvider.state('usersCreate', {
            url: '/users/create',
            templateUrl: '/templates/usersManager/usersCreate.html',
            controller: 'UsersCreateController'
        });

        /* Просмотр и редактирование информации о пользователе */
        $stateProvider.state('usersView', {
            url: '/users/{userId:[0-9]{1,}}',
            templateUrl: '/templates/usersManager/usersView.html',
            controller: 'UsersViewController'
        });


    });

/*
* Модуль с общими контроллерами приложения
* */
angular.module('app.controllers', [])
    .controller('PageController', ['$scope', '$auth', '$state', '$rootScope', '$location', 'userHandler', function($scope, $auth, $state, $rootScope, $location, userHandler) {
        NProgress.configure({ showSpinner: false });

        $scope.logout = function() {

            $auth.logout().then(function() {
                localStorage.removeItem('user');
                $rootScope.authenticated = false;
                $rootScope.currentUser = {};
                userHandler.clear();



                $location.path('/');
            });

        };

    }]);


angular.module('app.filters', [])
    .filter('parseDate', function() {
        return function(rawDate) {
            var date = rawDate.split(' ')[0].split('-');
            var day = date[2];
            var month = date[1];
            var year = date[0];

            return day + '.' + month + '.' + year;
        };
});
angular.module('app', [
    'ui.router',
    'auth',
    'publicationsManager',
    'usersManager',
    'supportManager',
    'sitesManager',
    'app.controllers',
    'app.filters'
])
    .config(function($provide, $httpProvider, $authProvider, $stateProvider, userHandlerProvider, $urlRouterProvider) {

        userHandlerProvider.load();

        /*
        * Автоматическая смена состояния, когда возвращается авторизованный пользователь
        * */
        if (userHandlerProvider.getCurrentUser()) {
            switch (userHandlerProvider.getCurrentUser().role) {
                case 'admin':
                    $urlRouterProvider.when('/', '/publications');
                    break;
                case 'user':
                case 'user-moderator':
                    $urlRouterProvider.when('/', '/publications/create');
                    break;
                case 'moderator':
                    $urlRouterProvider.when('/', '/publications');
                    break;
            }


        } else {
            $stateProvider.state('index', {
                url: '/'
            });
        }



        /* Для CORS на всякий случай */
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];


        function redirectWhenLoggedOut($q, $injector, $rootScope, $location) {


            return {

                'responseError': function(rejection) {

                    //var $location = $injector.get('$location');

                    var rejectReasons = [
                        'token_expired',
                        'token_invalid',
                        'token_absent',
                        'token_not_provided'
                    ];




                    angular.forEach(rejectReasons, function(value, key) {



                        if (rejection.data.error === value) {
                            localStorage.removeItem('user');
                            $rootScope.alertText = "Время сессии окончено. Перезайдите.";
                            $rootScope.showAlertBlock = true;
                            $rootScope.authenticated = false;
                            $rootScope.currentUser = null;
                            $location.url('/login');
                        }


                    });




                    return $q.reject(rejection);
                }


            };
        }



        $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);


        $httpProvider.interceptors.push('redirectWhenLoggedOut');

    })
    .run(function($rootScope, $location, userHandler) {

        $rootScope.$on('$stateChangeStart', function(event) {
            if (userHandler.get()) {
                $rootScope.authenticated = true;
            }
            NProgress.start();
        });

        
        $rootScope.$on('$stateChangeSuccess', function() {
            NProgress.done();
        });

        moment.locale('ru');

        if ($location.path() == "") {
            $location.path('/');
        }
    });




//# sourceMappingURL=siteAdmApp.js.map
