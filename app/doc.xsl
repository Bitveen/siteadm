<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
	
	<xsl:template match="w:body">
		<div>
			<xsl:apply-templates select="w:p" />
		</div>
	</xsl:template>

	
	<xsl:template match="w:p">
		<p>
			<xsl:attribute name="align">
				<xsl:apply-templates select="w:pPr[1]" />
			</xsl:attribute>
			<xsl:value-of select="." />
		</p>

	</xsl:template>

	<xsl:template match="w:pPr">
		<xsl:value-of select="w:jc/@w:val" />
	</xsl:template>




</xsl:stylesheet>