<?php namespace App;

use DB;

class Site {

    public static function create($url, $title)
    {
        return DB::table('sites')->insertGetId(
            ['url' => $url, 'title' => $title]
        );
    }

    public static function getAll()
    {
        return DB::table('sites')->select('id', 'url', 'title')->get();
    }

    public static function getURL($siteId)
    {
        return DB::table('sites')->select('url', 'id')->where('id', '=', $siteId)->get()[0];
    }


}
