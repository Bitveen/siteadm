<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use DB;


class User extends Model implements AuthenticatableContract
{
    use Authenticatable;

    protected $table = 'users';


    protected $fillable = ['password', 'login', 'role'];


    protected $hidden = ['password', 'remember_token'];


    public static function getAllUsers()
    {
        $query = "SELECT * FROM users WHERE role<>'admin' ORDER BY created_at DESC";
        return DB::select($query);
    }




    public static function addSite($userId, $site, $siteTitle)
    {
        $query = "INSERT INTO user_sites(url, userId, title) VALUES(?, ?, ?)";

        return DB::insert($query, [$site, $userId, $siteTitle]);

    }


    public static function getSites($userId)
    {
        $query = "SELECT id, title FROM user_sites WHERE userId=?";
        return DB::select($query, [$userId]);
    }


    public static function getSite($siteId)
    {
        return DB::select("SELECT url FROM user_sites WHERE id=?", [$siteId])[0];
    }

    public static function getData($userId)
    {
        return DB::table('users')->select('id', 'login', 'role', 'name', 'settlement')->where('id', '=', $userId)->get()[0];
    }



}
