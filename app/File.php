<?php namespace App;

use Carbon\Carbon;

class File {

    const DOCX_FILE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    const DOC_FILE  = "application/msword";
    const PDF_FILE  = "application/pdf";
    const XLS_FILE  = "application/vnd.ms-excel";
    const XLSX_FILE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    const JPEG_FILE = "image/jpeg";
    const PNG_FILE  = "image/png";
    const GIF_FILE  = "image/gif";


    public static function transliterate($name)
    {
        $name = mb_strtolower($name);

        $search = [
            ' ', '\'', '\"', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
            'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
        ];

        $replace = [
            '_', '', '', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n',
            'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sh', '', 'yi', '', 'ye', 'yu', 'ya'
        ];

        return str_replace($search, $replace, $name);
    }



    public static function makeName($fileNameRaw)
    {

        $fileName = self::transliterate($fileNameRaw);
        $h = Carbon::now('Europe/Moscow')->hour;
        $m = Carbon::now('Europe/Moscow')->minute;
        $s = Carbon::now('Europe/Moscow')->second;

        $fileName = $h."_".$m."_".$s."_".$fileName;

        return $fileName;
    }



    public static function getExtension($fileName)
    {
        $extension = explode('.', $fileName);
        $extension = $extension[count($extension) - 1];
        return $extension;
    }


    public static function createPath($type, $settlement)
    {

        $settlement = self::transliterate($settlement);

        $now = Carbon::today()->formatLocalized('%Y/%m/%d');

        $path = "";

        switch($type) {
            case 'tmp-pub':
                $path = __DIR__.'/../public/storage/tmp/publications/'.$now.'/'.$settlement;
                break;
            case 'tmp-sup':
                $path = __DIR__.'/../public/storage/tmp/support/'.$now.'/'.$settlement;
                break;
        }

        return $path;
    }



    public static function parseDocX($mainFilePath)
    {
        $zip = new \ZipArchive();
        $zip->open(__DIR__.'/../public/storage/tmp/publications/'.$mainFilePath);

        $content = $zip->getFromName('word/document.xml');
        $zip->close();


        $xml = new \DOMDocument();
        $xml->loadXML($content);

        $xsl = new \DOMDocument();
        $xsl->load(__DIR__.'/doc.xsl');

        $xslt = new \XSLTProcessor();
        $xslt->importStylesheet($xsl);

        return $xslt->transformToXML($xml);
    }


    public static function parseDoc($mainFilePath)
    {
        $tmpl = "";
        require_once __DIR__.'/cfb.php';
        require_once __DIR__.'/doc.php';
        $doc = new \doc;
        $doc->read(__DIR__.'/../public/storage/tmp/publications/'.$mainFilePath);
        $tmpl = mb_convert_encoding($doc->parse(), "UTF-8");

        if (mb_strlen($tmpl) == 0) {
            // это rtf
            require_once __DIR__.'/rtf.php';
            $tmpl = mb_convert_encoding(rtf2text(__DIR__.'/../public/storage/tmp/publications/'.$mainFilePath), "UTF-8");
        }

        return $tmpl;
    }
}
