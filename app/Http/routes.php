<?php

Route::get('/', function() {
    return view('index');
});


Route::group(['prefix' => 'api'], function() {

    /*
     * Маршрут для авторизации
     * */
    Route::post('authenticate', 'AuthController@authenticate');
    Route::get('authenticate/user', 'AuthController@getAuthenticatedUser');

    /*
     * Маршруты для работы с пользователями
     * */
    Route::group(['prefix' => 'users'], function() {

        Route::get('/', 'UsersController@index');
        Route::post('/', 'UsersController@create');
        Route::get('/{userId}', 'UsersController@show');
        Route::put('/{userId}', 'UsersController@update');

    });


    /*
     * Маршруты для работы с файлами
     * */
    Route::post('files', 'FilesController@index');


    /*
     * Маршруты для работы с рубриками
     * */
    Route::get('terms', 'TermsController@search');



    /*
     * Мапшруты для работы с публикациями
     * */
    Route::group(['prefix' => 'publications'], function() {

        Route::get('/', 'PublicationsController@index');
        Route::post('/', 'PublicationsController@create');
        Route::get('/{pubId}', 'PublicationsController@show');
        Route::put('/{pubId}', 'PublicationsController@approve');
        Route::post('/{pubId}', 'PublicationsController@drop');
    });


    /*
     * Маршруты для работы с заявками в тех. поддержку
     * */
    Route::group(['prefix' => 'support'], function() {

        Route::get('/', 'SupportController@index');
        Route::post('/', 'SupportController@create');
        Route::get('/{supId}', 'SupportController@show');
        Route::put('/{supId}', 'SupportController@check');
        Route::put('/color/{supId}', 'SupportController@changeColor');
    });


    /*
     * Маршруты для работы с менеджером сайтов
     * */
    Route::group(['prefix' => 'sites'], function() {
        Route::get('/', 'SitesController@index');
        Route::post('/', 'SitesController@create');
        Route::get('/{siteId}', 'SitesController@getURL');
    });



});