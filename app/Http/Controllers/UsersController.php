<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;


class UsersController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }



    public function index()
    {
        $users = User::getAllUsers();
        return $users;
    }



    public function show($userId)
    {
        $user = User::getData($userId);
        $sites = User::getSites($userId);
        return response()->json(compact('user', 'sites'));
    }



    public function create(Request $request)
    {
        $user = new User();
        $user->login = $request->input('login');
        $user->password = bcrypt($request->input('password'));
        $user->role = $request->input('role');


        if ($request->has('name')) {
            $user->name = $request->input('name');
        }


        if ($request->has('settlement')) {
            $user->settlement = $request->input('settlement');
        }

        if ($user->save()) {
            if ($request->has('site')) {
                $site = $request->input('site');
                $siteTitle = $request->input('siteTitle');
                if (User::addSite($user->id, $site, $siteTitle)) {
                    return json_encode(['status' => 'Created']);
                }
            }
        }
    }

    public function update($userId, Request $request)
    {

    }


}
