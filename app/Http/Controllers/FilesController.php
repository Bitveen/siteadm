<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\File;

use Storage;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;



class FilesController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function index(Request $request)
    {
        $action = $request->get('action');

        switch ($action) {

            case 'upload':

                $document = $request->file('file');

                $pathToFile = "";

                $fileName = File::makeName($document->getClientOriginalName());

                if ($request->has('type')) {
                    switch($request->get('type')) {
                        case 'publication':
                            $pathToFile = File::createPath('tmp-pub', JWTAuth::parseToken()->authenticate()->settlement);
                            break;
                        case 'support':
                            $pathToFile = File::createPath('tmp-sup', JWTAuth::parseToken()->authenticate()->settlement);
                            break;
                    }
                }


                if ($document->move($pathToFile, $fileName)) {

                    $pathToSend = explode('/', $pathToFile.'/'.$fileName);
                    $pathToSend = array_slice($pathToSend, -5);
                    $pathToSend = implode('/', $pathToSend);

                    return response()->json(['status' => 'uploaded', 'path' => $pathToSend, 'type' => $document->getClientMimeType()]);
                }










                break;
            case 'drop':

                if ($request->has('type')) {

                    switch($request->get('type')) {

                        case 'support':
                            $path = $request->get('filePath');
                            $path = "tmp/support/".$path;
                            break;

                        case 'publication':
                            $path = $request->get('filePath');
                            $path = "tmp/publications/".$path;
                            break;

                    }

                    if (Storage::delete($path)) {
                        return response()->json(['status' => 'deleted'], 200);
                    }






                }
                break;
        }











    }




}
