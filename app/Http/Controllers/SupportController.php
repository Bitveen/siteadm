<?php namespace App\Http\Controllers;

use App\Support;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Storage;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class SupportController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $type = $request->get('type');
        $role = JWTAuth::parseToken()->authenticate()->role;
        $supportItems = [];

        $supportItems = Support::all();


        /*switch ($role) {
            case 'admin':

                switch ($type) {
                    case 'posted':
                        $supportItems = Support::getPostedItems();
                        break;
                    case 'processed':
                        $supportItems = Support::getProcessedItems();
                        break;
                }

                break;
            case 'user':

                switch ($type) {
                    case 'posted':
                        $supportItems = Support::getPostedItemsByUser(JWTAuth::parseToken()->authenticate()->id);
                        break;
                    case 'processed':
                        $supportItems = Support::getProcessedItemsByUser(JWTAuth::parseToken()->authenticate()->id);
                        break;
                }

                break;

        }*/

        return response()->json($supportItems);
    }



    public function create(Request $request)
    {

        $supportData['title'] = $request->get('title');
        $supportData['body'] = $request->get('body');
        $supportData['status'] = "send";

        $supportData['color'] = 'default';
        $supportData['created_at'] = Carbon::now('Europe/Moscow')->toDateTimeString();
        $supportData['updated_at'] = Carbon::now('Europe/Moscow')->toDateTimeString();
        $supportData['userId'] = JWTAuth::parseToken()->authenticate()->id;

        $supportFiles = $request->get('files');


        if ($id = Support::createSupportRequest($supportData, $supportFiles)) {
            foreach ($supportFiles as $file) {
                Storage::move('tmp/support/'.$file['path'], 'support/'.$file['path']);
            }
            return response()->json(['status' => 'Created', 'id' => $id, 'created_at' => $supportData['created_at']], 200);
        } else {
            return response()->json(['status' => 'Error'], 500);
        }

    }



    public function show($supId, Request $request)
    {
        $supportData = Support::getOneItem($supId);
        $supportFiles = Support::getFiles($supId);
        return response()->json(compact('supportData', 'supportFiles'));
    }


    public function check($supId, Request $request)
    {
        if (Support::setChecked($supId)) {
            return response()->json(['status' => 'Updated'], 200);
        } else {
            return response()->json(['status' => 'Error'], 500);
        }
    }


    public function changeColor($supId, Request $request)
    {

        $color = $request->input('color');

        if (Support::updateColor($supId, $color)) {
            return response()->json(['status' => 'changed'], 200);
        } else {
            return response()->json(['status'=> 'error'], 500);
        }

    }


}
