<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class SitesController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function index()
    {
        $sites = Site::getAll();
        return response()->json(compact('sites'));
    }

    public function create(Request $request)
    {
        $url = $request->get('url');
        $title = $request->get('title');

        if ($id = Site::create($url, $title)) {
            return response()->json(['status' => 'Created', 'id' => $id], 200);
        } else {
            return response()->json(['status' => 'Error'], 500);
        }
    }

    public function getURL($siteId)
    {
        $siteURL = Site::getURL($siteId);
        return response()->json(compact('siteURL'));
    }




}
