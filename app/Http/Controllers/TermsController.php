<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Term;


class TermsController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function search(Request $request)
    {
        $search = "%".$request->get('search')."%";
        $siteId = $request->get('siteId');
        return response()->json(Term::getTermsBySite($siteId, $search));
    }



}
