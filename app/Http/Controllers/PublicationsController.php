<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Publication;
use Storage;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\File;
use Carbon\Carbon;

class PublicationsController extends Controller {

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function index(Request $request)
    {
        $publications = [];


        $publications = Publication::all();


        /*switch(JWTAuth::parseToken()->authenticate()->role) {

            case 'user':

                if ($request->has('type')) {

                    switch($request->get('type')) {
                        case 'archive':
                            $publications = Publication::getArchived(JWTAuth::parseToken()->authenticate()->id);
                            break;
                    }

                } else {

                    $publications = Publication::getAllInMonth(JWTAuth::parseToken()->authenticate()->id);

                }
                break;

            case 'admin':

                switch($request->get('type')) {

                    case 'processed':
                        $publications = Publication::getProcessed();

                        break;
                    case 'posted':

                        $publications = Publication::getPosted();

                        break;

                }

                break;

            case 'moderator':

                $publications = Publication::getAllActivePublications();

                break;
        }*/

        return response()->json($publications);
    }


    public function show($pubId)
    {
        switch(JWTAuth::parseToken()->authenticate()->role) {
            case 'user':
                //показать свою одну
                $publication = Publication::getOneActivePublicationById($pubId);
                $files = Publication::getFilesByPubId($pubId);
                return response()->json(compact('publication', 'files'));
                break;
            case 'admin':
                //показать любую одну
                $publication = Publication::getOneActivePublicationById($pubId);
                $userId = $publication->userId;
                $sites = User::getSites($userId);
                $files = Publication::getFilesByPubId($pubId);
                return response()->json(compact('publication', 'files', 'sites'));
                break;
            case 'moderator':
                //показать любую одну
                $publication = Publication::getOneActivePublicationById($pubId);
                $userId = $publication->userId;
                $sites = User::getSites($userId);
                $files = Publication::getFilesByPubId($pubId);
                return response()->json(compact('publication', 'files', 'sites'));
                break;
        }

    }


    public function create(Request $request)
    {

        $publication['title'] = $request->input('title');

        if ($request->has('comment')) {
            $publication['comment'] = $request->input('comment');
        } else {
            $publication['comment'] = null;
        }

        $publication['pubDate'] = Carbon::createFromFormat('d.m.Y', $request->input('pubDate'))->toDateString();
        $publication['status'] = 'moderation';
        $publication['userId'] = JWTAuth::parseToken()->authenticate()->id;
        $publication['created_at'] = Carbon::now('Europe/Moscow')->toDateTimeString();
        $publication['updated_at'] = Carbon::now('Europe/Moscow')->toDateTimeString();


        // files - двумерный массив



        $filesRequest = $request->input('files');

        $mainFilePath = "";
        $mainFileMimeType = "";


        foreach ($filesRequest as $file) {
            if ($file['isMain']) {
                $mainFilePath = $file['path'];
                $mainFileMimeType = $file['mimeType'];
                break;
            }
        }






        $template = "";
        foreach ($filesRequest as $file) {
            if ($file['isMain']) {
                continue;
            }
            switch($file['mimeType']) {
                case File::JPEG_FILE:
                    $template .= "<p style='text-align: center;'><img src='http://siteadm.pro/storage/publications/" . $file['path'] . "'></p>";
                    break;
                case File::GIF_FILE:
                    $template .= "<p style='text-align: center;'><img src='http://siteadm.pro/storage/publications/" . $file['realName'] . "'></p>";
                    break;
                case File::PNG_FILE:
                    $template .= "<p style='text-align: center;'><img src='http://siteadm.pro/storage/publications/" . $file['realName'] . "'></p>";
                    break;
            }
        }




        if ($mainFilePath) {
            //основной файл установлен
            switch($mainFileMimeType) {
                case File::DOCX_FILE:
                    $template .= File::parseDocX($mainFilePath);
                    break;
                case File::DOC_FILE:
                    $template .= File::parseDoc($mainFilePath);
                    break;
            }
        }



        $publication['content'] = $template;

        if ($id = Publication::createNew($publication, $filesRequest)) {
            return response()->json(['id' => $id, 'status' => 'OK']);
        } else {
            return response()->json(['status' => 'error'], 500);
        }



    }


    public function approve($pubId, Request $request)
    {

        $data['title'] = $request->input('title');
        $data['updated_at'] = Carbon::now('Europe/Moscow')->toDateTimeString();
        $data['content'] = $request->input('content');
        $data['pubDate'] = Carbon::createFromFormat('d.m.Y', $request->input('pubDate'))->toDateString();
        $data['status'] = 'published';
        $data['chosenTerms'] = $request->input('chosenTerms'); // двумерный массив
        $siteId = $request->input('site')['id'];
        $data['userId'] = $request->input('userId');
        $userSite = User::getSite($siteId)->url;


        Publication::insertTermsByPublication($pubId, $data['chosenTerms']);





        $files = Publication::getFilesByPubId($pubId);


        for ($i = 0; $i < count($files); $i++) {
            Storage::move('tmp/publications/'.$files[$i]->path, 'publications/'.$files[$i]->path);
            $fileType = $files[$i]->type;
            switch ($fileType) {
                case File::JPEG_FILE:
                case File::GIF_FILE:
                case File::PNG_FILE:
                    unset($files[$i]);
            }
        }



        $post['title'] = $data['title'];
        $post['pubDate'] = $data['pubDate'];
        $post['content'] = $data['content'];
        $post['terms'] = json_encode($data['chosenTerms']);
        $post['files'] = json_encode($files);
        $ch = curl_init($userSite.'pubCreator.php');

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        if(curl_exec($ch)) {


            if (Publication::publish($data, $pubId, $siteId)) {
                return json_encode(['status' => 'Published']);
            }

        } else {
            abort(500);
        }

        curl_close($ch);



    }


    public function drop($pubId)
    {
        if(Publication::drop($pubId)) {
            return response()->json(['status' => 'Deleted'], 200);
        } else {
            return response()->json(['status' => 'Error'], 500);
        }
    }





}
