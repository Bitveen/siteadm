<?php namespace App;

use DB;
use Carbon\Carbon;


class Support {

    public static function getPostedItemsByUser($userId)
    {
        $query = "SELECT * FROM support WHERE status='send' AND userId=? ORDER BY created_at DESC";
        return DB::select($query, [$userId]);
    }

    public static function getProcessedItemsByUser($userId)
    {
        $query = "SELECT * FROM support WHERE status='processed' AND userId=? ORDER BY created_at DESC";
        return DB::select($query, [$userId]);
    }



    public static function getProcessedItems()
    {
        $query = "SELECT users.settlement, support.title, support.updated_at, support.color, support.id FROM support INNER JOIN users ON users.id=support.userId WHERE status='processed' ORDER BY support.created_at DESC";
        return DB::select($query);
    }

    public static function getPostedItems()
    {
        $query = "SELECT users.settlement, support.title, support.created_at, support.color, support.id FROM support INNER JOIN users ON users.id=support.userId WHERE status='send' ORDER BY support.created_at DESC";
        return DB::select($query);
    }

    public static function all() {
        return DB::table('support')
            ->join('users', 'users.id', '=', 'support.userId')
            ->select('users.settlement', 'support.title', 'support.id', 'support.color', 'support.created_at', 'support.status')
            ->orderBy('support.status', 'desc')
            ->get();
    }






    public static function createSupportRequest($supportData, $supportFiles)
    {

        $id = DB::table('support')->insertGetId($supportData);


        if ($id) {

            if (!empty($supportFiles)) {

                $filesQuery = "INSERT INTO support_files(name, path, supId, created_at, updated_at) VALUES ";
                foreach ($supportFiles as $file) {
                    $filePath = $file['path'];
                    $fileName = $file['realName'];
                    $date = Carbon::now('Europe/Moscow')->toDateTimeString();
                    $filesQuery .= "('$fileName', '$filePath', '$id', '$date', '$date'), ";
                }
                $filesQuery = mb_substr($filesQuery, 0, mb_strlen($filesQuery) - 2);
                DB::insert($filesQuery);

            }
            return $id;
        }


    }


    public static function getAllSupportByUser($userId)
    {
        $query = "SELECT * FROM support WHERE userId=?";
        return DB::select($query, [$userId]);

    }


    public static function getAllWithUser()
    {
        $query = "SELECT users.login, support.title, support.id, support.status, support.created_at, support.color ".
            "FROM support INNER JOIN users ON users.id = support.userId WHERE status='send' ORDER BY support.created_at DESC";
        return DB::select($query);
    }

    public static function getOneItem($supId)
    {
        $query = "SELECT users.settlement, support.title, support.id, support.body, support.status, support.created_at ".
            "FROM support INNER JOIN users ON users.id = support.userId WHERE support.id=? ORDER BY support.created_at DESC";
        return DB::select($query, [$supId])[0];
    }

    public static function getFiles($supId)
    {
        $query = "SELECT * FROM support_files WHERE supId=?";
        return DB::select($query, [$supId]);
    }

    public static function setChecked($supId)
    {
        $query = "UPDATE support SET status='processed' WHERE id=?";
        return DB::update($query, [$supId]);
    }

    public static function updateColor($supId, $color)
    {
        $query = "UPDATE support SET color=? WHERE id=?";
        return DB::update($query, [$color, $supId]);
    }

    public static function unprocessedCount()
    {
        return DB::table('support')->where('status', '=', 'send')->count();
    }
}
