<?php namespace App;

use DB;
use App\File;

class Publication {


    /**
     * Метод для возврата всех публикаций из базы(для админа и модератора)
     *
     * @return mixed
     */
    public static function all() {
        return DB::table('publications')
            ->join('users', 'users.id', '=', 'publications.userId')
            ->select('users.settlement', 'publications.status', 'publications.title', 'publications.id', 'publications.updated_at')
            ->orderBy('publications.status', 'asc')->orderBy('publications.updated_at', 'desc')->get();
    }


    public static function getProcessed()
    {
        return DB::table('publications')
            ->join('users', 'users.id', '=', 'publications.userId')
            ->select('users.settlement', 'publications.title', 'publications.id', 'publications.updated_at')
            ->where('publications.status', '=', 'published')->orderBy('publications.updated_at', 'desc')->get();
    }


    public static function getPosted()
    {
        return DB::table('publications')
            ->join('users', 'users.id', '=', 'publications.userId')
            ->select('users.settlement', 'publications.title', 'publications.id', 'publications.updated_at')
            ->where('publications.status', '=', 'moderation')->orderBy('publications.updated_at', 'desc')->get();
    }



    public static function createNew($publication, $files)
    {

        $id = DB::table('publications')->insertGetId($publication);

        if ($id) {

            if (!empty($files)) {

                $filesQuery = "INSERT INTO files(name, path, isMain, pubId, type) VALUES ";
                foreach ($files as $file) {
                    $filePath = $file['path'];
                    $fileName = $file['realName'];
                    $fileIsMain = $file['isMain'];
                    $fileType = $file['mimeType'];
                    $filesQuery .= "('$fileName', '$filePath', '$fileIsMain', '$id', '$fileType'), ";
                }
                $filesQuery = mb_substr($filesQuery, 0, mb_strlen($filesQuery) - 2);
                DB::insert($filesQuery);

            }
            return $id;
        }


    }


    public static function updateOne($publication, $pubId, $files)
    {
        $updateQuery = "UPDATE publications SET title=?, pubDate=?, content=?, comment=?, updated_at=? WHERE id=?";
        if (DB::update($updateQuery, [
            $publication['title'],
            $publication['pub_date'],
            $publication['content'],
            $publication['comment'],
            $publication['updated_at'],
            $pubId
        ])) {

            $deleteQuery = "DELETE FROM files WHERE pubId=?";
            if (DB::delete($deleteQuery, [$pubId])) {

                if (!empty($files)) {

                    $filesQuery = "INSERT INTO files(name, path, isMain, pubId) VALUES ";

                    foreach ($files as $index => $file) {
                        $filePath = $file['path'];
                        $fileName = $file['name'];
                        $fileIsMain = $file['isMain'];
                        $filesQuery .= "('$fileName', '$filePath', '$fileIsMain', '$pubId'), ";
                    }
                    $filesQuery = mb_substr($filesQuery, 0, mb_strlen($filesQuery) - 2);
                    DB::insert($filesQuery);

                }



            }


        }



    }



    public static function getAllInMonth($userId)
    {
        return DB::table('publications')
            ->select('title', 'id', 'pubDate', 'status')
            ->where('userId', '=', $userId)->orderBy('updated_at', 'desc')->orderBy('status')->get();
    }





    public static function getArchived($userId)
    {
        $query = "SELECT * FROM publications WHERE userId=? AND status='published' AND (CURDATE() > DATE_ADD('updated_at', INTERVAL 30 DAY)) ORDER BY created_at DESC";
        return DB::select($query, [$userId]);
    }


    public static function getOne($pubId, $userId)
    {
        $query = "SELECT * FROM publications WHERE userId=? AND id=?";
        return DB::select($query, [$userId, $pubId]);
    }

    public static function getFilesByPubId($pubId)
    {
        $query = "SELECT * FROM files WHERE pubId=?";
        return DB::select($query, [$pubId]);
    }

    public static function getAllActivePublications()
    {
        $query = "SELECT users.login, users.settlement, publications.title, publications.id, publications.updated_at FROM publications INNER JOIN users ".
            "ON users.id=publications.userId WHERE publications.status='moderation' ORDER BY updated_at DESC";
        return DB::select($query);

    }

    public static function getOneActivePublicationById($pubId)
    {
        $query = "SELECT users.settlement, users.id as userId, publications.id, publications.title, publications.pubDate, publications.updated_at, ".
            "publications.comment, publications.status, publications.content FROM publications INNER JOIN users ON users.id=publications.userId WHERE publications.id=?";
        return DB::select($query, [$pubId])[0];
    }


    public static function publish($data, $pubId, $siteId)
    {
        $query = "UPDATE publications SET title=?, updated_at=?, status=?, pubDate=?, content=?, siteId=? WHERE id=?";
        return DB::update($query, [
            $data['title'],
            $data['updated_at'],
            $data['status'],
            $data['pubDate'],
            $data['content'],
            $siteId,
            $pubId
        ]);
    }



    public static function insertTermsByPublication($pubId, $terms)
    {
        foreach ($terms as $term) {
            DB::table('publication_terms')->insert([
                'pubId' => $pubId,
                'termId' => $term['id']
            ]);
        }



    }

    public static function drop($pubId)
    {
        return DB::table('publications')->where('id', '=', $pubId)->delete();
    }





    public static function allCounter()
    {
        return DB::table('publications')->count();
    }

    public static function unprocessedCounter()
    {
        return DB::table('publications')->where('status', '=', 'moderation')->count();
    }

}
