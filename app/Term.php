<?php namespace App;

use DB;

class Term {
    public static function getTermsBySite($siteId, $search)
    {
        $query = "SELECT name, realId, id FROM terms WHERE siteId=? AND name LIKE ?";
        return DB::select($query, [$siteId, $search]);
    }
}
