/*
* Модуль с сервисами для менеджера сайтов
* */
angular.module('sitesManager.services', [])
    .factory('sitesHandler', function() {

        var site = {};

        return {
            get: function() {
                return site;
            },
            set: function(siteData) {
                site = siteData;
            }
        };



    })
    .factory('siteInfoService', function($http, $stateParams, sitesHandler) {


        return {
            update: function(info) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/info',
                    method: 'POST',
                    skipAuthorization: true,
                    data: info,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            }

        };
    })
    .factory('siteLayoutService', function(sitesHandler, $stateParams, $http) {
        return {
            setMenuItem: function(item) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/main_menu/' + item.id + '?is_active=' + item.is_active,
                    method: 'POST',
                    skipAuthorization: true,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            },
            updateLayoutHeader: function(layout) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/layout?section=header',
                    method: 'POST',
                    skipAuthorization: true,
                    data: layout,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            },
            updateLayoutFooter: function(layout) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/layout?section=footer',
                    method: 'POST',
                    skipAuthorization: true,
                    data: layout,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            },
            updateLayoutContainer: function(layout) {
                return $http({
                    url: sitesHandler.get($stateParams.siteId).url + 'api/layout?section=container',
                    method: 'POST',
                    skipAuthorization: true,
                    data: layout,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                });
            }

        };
    })
    .factory('siteStyleService', function($http, sitesHandler) {

        return {
            save: function(style) {
                return $http({
                    url: sitesHandler.get().url + 'api/style',
                    method: 'POST',
                    skipAuthorization: true,
                    data: style.body,
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                })
            }
        };

    })
    .factory('sitePublicationsService', function($http, sitesHandler, $stateParams) {

        var _update = function(publication) {
            return $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/publications/' + publication.id,
                method: 'POST',
                data: publication,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };
        var _delete = function(id) {
            return $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/publications/' + publication.id + '?delete=1',
                method: 'POST',
                data: {},
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };


        return {
            update: _update,
            delete: _delete
        };
    })
    .factory('siteScriptsService', function($http, sitesHandler) {
        var _update = function(data) {
            return $http({
                url: sitesHandler.get().url + 'api/scripts',
                method: 'POST',
                data: data,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };
        return {
            update: _update
        };
    })
    .factory('sitePagesService', function($http, sitesHandler, $stateParams) {

        var _create = function(pageData) {
            return $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/pages',
                method: 'POST',
                data: pageData,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };


        return {
            create: _create
        };

    })
    .factory('siteHeaderService', function($http, sitesHandler) {
        /* Сервис для работы с хедером сайта */
        var _save = function(data) {
            return $http({
                url: sitesHandler.get().url + 'api/header',
                method: 'POST',
                data: data,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };

        return {
            save: _save
        };

    })
    .factory('siteFooterService', function($http, sitesHandler) {
        /* Сервис для работы с футером сайта */
        var _save = function(data) {
            return $http({
                url: sitesHandler.get().url + 'api/footer',
                method: 'POST',
                data: data,
                skipAuthorization: true,
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
        };

        return {
            save: _save
        };

    }).factory('siteCreateService', function($http) {
        var _create = function(site) {
            return $http.post('/api/sites', site);
        };

        return {
            create: _create
        };


    });