/*
* Модуль для управления сайтами
* */
angular.module('sitesManager', ['sitesManager.controllers', 'sitesManager.directives', 'sitesManager.services', 'sitesManager.filters'])
    .config(function($stateProvider) {

        /* Список всех сайтов */
        $stateProvider.state('sitesList', {
            url: '/sites',
            templateUrl: '/templates/sitesManager/sitesList.html',
            resolve: {
                sitesList: function($http) {
                    return $http.get('/api/sites');
                }
            },
            controller: 'SitesListController'
        });

        /* Базовый шаблон для просмотра сайта, включает себя меню вспомогательное */
        $stateProvider.state('sitesView', {
            url: '/sites/{siteId:int}',
            templateUrl: '/templates/sitesManager/sitesView.html'
        });

        /* Страница с просмотром и редактированием основной информации о поселении */
        $stateProvider.state('sitesView.info', {
            url: '/info',
            templateUrl: '/templates/sitesManager/sitesViewInfo.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                info: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/info', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesInfoController'
        });

        /* Главная страница сайта */
        $stateProvider.state('sitesView.mainPage', {
            url: '/main',
            templateUrl: '/templates/sitesManager/sitesViewMainPage.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                widgets: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/widgets', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesMainPageController'
        });

        /* Публикации сайта */
        $stateProvider.state('sitesView.publications', {
            url: '/publications',
            templateUrl: '/templates/sitesManager/sitesViewPublications.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                publications: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/publications', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPublicationsController'
        });


        /* Просмотр и редактирование одной публикации */
        $stateProvider.state('sitesView.publicationsView', {
            url: '/publications/{publicationId:int}',
            templateUrl: '/templates/sitesManager/sitesViewPublicationsView.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                publication: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/publications/' + $stateParams.publicationId, {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPublicationsViewController'
        });


        /* Просмотр и изменение стилей сайта */
        $stateProvider.state('sitesView.styles', {
            url: '/styles',
            templateUrl: '/templates/sitesManager/sitesViewStyles.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                style: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/style', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesStylesController'
        });


        /* Просмотр и изменение разметки сайта */
        $stateProvider.state('sitesView.layout', {
            url: '/layout',
            templateUrl: '/templates/sitesManager/sitesViewLayout.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                layout: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/layout', {
                        skipAuthorization: true
                    });
                },
                menu: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/main_menu', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesLayoutController'
        });

        /* Просмотр страниц сайта */
        $stateProvider.state('sitesView.pages', {
            url: '/pages',
            templateUrl: '/templates/sitesManager/sitesViewPages.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                pages: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/pages', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPagesController'
        });

        /* Просмотр одной страницы сайта */
        $stateProvider.state('sitesView.pagesView', {
            url: '/pages/{remoteId:int}',
            templateUrl: '/templates/sitesManager/sitesViewPagesView.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                page: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/pages/' + $stateParams.remoteId, {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPagesViewController'
        });


        /* Создание новой страницы сайта */
        $stateProvider.state('sitesView.pagesCreate', {
            url: '/pages/create',
            templateUrl: '/templates/sitesManager/sitesViewPagesCreate.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                terms: function($http, $stateParams, sitesHandler) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/terms', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesPagesCreateController'
        });

        /* Просмотр рубрик сайта */
        $stateProvider.state('sitesView.terms', {
            url: '/terms',
            templateUrl: '/templates/sitesManager/sitesViewTerms.html',
            resolve: {
                /*sitesHandler: 'sitesHandler',
                terms: function($http, sitesHandler, $stateParams) {
                    return $http.get(sitesHandler.get($stateParams.siteId).url + 'api/terms', {
                        skipAuthorization: true
                    });
                }*/
            },
            controller: 'SitesTermsController'
        });


        /* Просмотр и редактирование скриптов сайта */
        $stateProvider.state('sitesView.scripts', {
            url: '/scripts',
            templateUrl: '/templates/sitesManager/sitesViewScripts.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                scripts: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);
                    return $http.get(siteUrl.data.siteURL.url + 'api/scripts', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesScriptsController'
        });




        /* Просмотр и редактирование футера сайта */
        $stateProvider.state('sitesView.footer', {
            url: '/footer',
            templateUrl: '/templates/sitesManager/sitesViewFooter.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                footer: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/footer', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesFooterController'
        });

        /* Просмотр и редактирование хедера сайта */
        $stateProvider.state('sitesView.header', {
            url: '/header',
            templateUrl: '/templates/sitesManager/sitesViewHeader.html',
            resolve: {
                siteUrl: function($http, $stateParams) {
                    return $http.get('/api/sites/' + $stateParams.siteId);
                },
                header: function($http, siteUrl, sitesHandler) {
                    sitesHandler.set(siteUrl.data.siteURL);

                    return $http.get(siteUrl.data.siteURL.url + 'api/header', {
                        skipAuthorization: true
                    });
                }
            },
            controller: 'SitesHeaderController'
        });


        /* Редактирование основного меню сайта */
        $stateProvider.state('sitesView.menu', {
            url: '/menu',
            templateUrl: '/templates/sitesManager/sitesViewMenu.html',
            resolve: {},
            controller: 'SitesMenuController'
        });





        /* Форма для созания нового сайта */
        $stateProvider.state('sitesCreate', {
            url: '/sites/create',
            templateUrl: '/templates/sitesManager/sitesCreate.html',
            controller: 'SitesCreateController'
        });

    })
    /*.run(function($rootScope, sitesHandler, $stateParams) {


        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {

            if (fromState.name === 'baseAdmin.sitesManager') {
                var sites = sitesHandler.getAll();

                sites.forEach(function(item) {
                    if (item.checked) {
                        delete item.checked;
                    }
                });


            }

        });

    })*/;
