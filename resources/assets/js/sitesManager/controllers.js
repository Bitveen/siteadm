/*
* Модуль с контроллерами для менеджера сайтов
* */
angular.module('sitesManager.controllers', ['ui.codemirror'])
    .controller('SitesListController', ['$scope', '$document', 'sitesList', function($scope, $document, sitesList) {

        $scope.sites = sitesList.data.sites;
        console.log(sitesList);

        $document[0].title = 'Менеджер сайтов';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.uncheckAll = function() {

            for (var i = 0; i < $scope.sites.length; i++) {
                if (!$scope.sites[i].checked) {
                    continue;
                }
                $scope.sites[i].checked = false;
                $scope.checkedCounter--;
            }

        };

        $scope.save = function(content) {

            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    $http({
                        url: $scope.sites[i].url + 'api/additional_content?type=' + $scope.currentAction,
                        method: 'POST',
                        skipAuthorization: true,
                        data: content,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).success(function() {
                        $.notify('Сохранено', 'success');
                        console.log('Success!');
                    });
                }

            }

        };

        $scope.term = null;
        $scope.currentAction = null;
        $scope.showActionsArea = false;

        $scope.checkedCounter = 0;

        $scope.choseAll = function() {

            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    continue;
                }
                $scope.sites[i].checked = true;
                $scope.checkedCounter++;
            }


        };

        $scope.setChecked = function (site) {
            if (site.checked) {
                site.checked = false;
                $scope.checkedCounter--;
                if ($scope.checkedCounter == 0) {
                    $scope.term = null;
                    $scope.currentAction = null;
                }

            } else {
                site.checked = true;
                $scope.checkedCounter++;
            }

        };


        $scope.changeTermTitle = function(term) {
            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    $http({
                        url: $scope.sites[i].url + 'api/terms',
                        method: 'POST',
                        skipAuthorization: true,
                        data: term,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).success(function() {
                        $.notify('Сохранено', 'success');
                        console.log('Success!');
                    });
                }

            }
        };

        $scope.changeAdditionalContent = function(content) {

            for (var i = 0; i < $scope.sites.length; i++) {
                if ($scope.sites[i].checked) {
                    $http({
                        url: $scope.sites[i].url + 'api/additional_content?type=' + $scope.currentAction,
                        method: 'POST',
                        skipAuthorization: true,
                        data: content,
                        headers: {
                            'Content-Type': 'text/plain'
                        }
                    }).success(function() {
                        $.notify('Сохранено', 'success');
                    }).error(function() {
                        $.notify('Возникла ошибка!', 'error');
                    });
                }

            }
        };


    }])
    .controller('SitesCreateController', ['$scope', '$document', 'siteCreateService', function($scope, $document, siteCreateService) {
        /* Контроллер для создания сайта */
        $document[0].title = 'Создание сайта';

        $scope.createSite = function(site) {
            siteCreateService.create(site).success(function() {
                $.notify('Сайт успешно создан!', 'success');
            }).error(function() {
                $.notify('Ошибка при создании сайта!', 'error');
            });
        };
    }])
    .controller('SitesInfoController', ['$scope', 'info', '$document', 'siteInfoService', '$rootScope', 'sitesHandler', '$stateParams', function($scope, info, $document, siteInfoService, $rootScope, sitesHandler, $stateParams) {
        /* Контроллер для отображения и заполнения основной информации о поселении */
        $document[0].title = 'Менеджер сайтов';

        $scope.siteInfo  = info.data;

        $scope.save = function(siteInfo) {
            siteInfoService.update(siteInfo).success(function() {
                $.notify('Сохранено!', 'success');
            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };


    }])
    .controller('SitesStylesController', ['$scope', '$document', 'style', 'siteStyleService', function($scope, $document, style, siteStyleService) {
        /* Контроллер для управления стилями сайта */
        $document[0].title = 'Стили сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.style = {};

        $scope.style.body = style.data;

        $scope.save = function(style) {
            siteStyleService.save(style).success(function() {

                $.notify('Сохранено!', 'success');

            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };

    }])
    .controller('SitesPagesController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для отображения страниц сайта */
        $document[0].title = 'Страницы сайта';
        $scope.pages = [];

        /*$scope.siteId = $stateParams.siteId;
        $scope.pages = pages.data;

        $scope.formData = {};

        $scope.showForm = false;
        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };


        $scope.showPage = function(id) {
            $location.path('admin/sites/' + $scope.siteId + '/pages/' + id);
        };

        $scope.showPageForm = function() {
            $scope.showForm = true;
        };

        $scope.cancelForm = function() {
            $scope.showForm = false;
            $scope.formData = null;
        };
*/
    }])
    .controller('SitesLayoutController', ['$scope', 'layout', '$document', 'menu', 'siteLayoutService', function($scope, layout, $document, menu, siteLayoutService) {

        $document[0].title = 'Разметка сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.layout = {};


        $scope.layout.header = layout.data.header;
        $scope.layout.footer = layout.data.footer;
        $scope.layout.container = layout.data.container;

        $scope.menu = {};
        $scope.menu.items = menu.data.menu;

        $scope.saveLayoutHeader = function(layout) {
            siteLayoutService.updateLayoutHeader(layout).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

        $scope.saveLayoutFooter = function(layout) {
            siteLayoutService.updateLayoutFooter(layout).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

        $scope.saveLayoutContainer = function(layout) {
            siteLayoutService.updateLayoutContainer(layout).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

        $scope.setActiveItem = function(item) {
            item.is_active = true;
            siteLayoutService.setMenuItem(item).success(function() {
                $.notify('Сохранено!', 'success');
            });

        };


        $scope.setNonActiveItem = function(item) {
            item.is_active = false;
            siteLayoutService.setMenuItem(item).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

    }])
    .controller('SitesPagesViewController', ['$scope', 'page', '$http', 'sitesHandler', '$stateParams', '$document', function($scope, page, $http, sitesHandler, $stateParams, $document) {

        $scope.content = page.data;
        $scope.siteId = $stateParams.siteId;

        $document[0].title = "Страницы сайта";

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };


        $scope.save = function(content) {
            $http({
                url: sitesHandler.get($stateParams.siteId).url + 'api/pages/' + $stateParams.remoteId,
                method: 'POST',
                skipAuthorization: true,
                data: content,
                headers: {
                    'Content-Type': 'text/plain'
                }
            }).success(function() {
                $.notify('Сохранено!', 'success');
            });
        };

    }])
    .controller('SitesMainPageController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для управления основной страницей сайта и виджетами */
        $document[0].title = 'Основная страница сайта';



    }])
    .controller('SitesPublicationsController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для отображения публикаций с сайта */
        $document[0].title = 'Список публикаций';
        $scope.publications = [];



        /*$scope.showPublication = function(id) {
            $location.path('/admin/sites/' + $stateParams.siteId + '/publications/' + id);
        };*/



    }])



    .controller('SitesTermsController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для управления рубриками */
        $document[0].title = 'Рубрики сайта';

        //$scope.terms = terms.data;

        $scope.terms = [];



    }])
    .controller('SitesPublicationsViewController', ['$scope', 'publication', 'sitePublicationsService', function($scope, publication, sitePublicationsService) {

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.publication = publication.data;

        $scope.savePublication = function(publication) {
            sitePublicationsService.update(publication).success(function(data) {
                $.notify('Сохранено!', 'success');
            });
        };

    }])

    .controller('SitesScriptsController', ['$scope', '$document', 'scripts', 'siteScriptsService', function($scope, $document, scripts, siteScriptsService) {
        /* Контроллер для управления скриптами сайта */
        $document[0].title = 'Скрипты сайта';

        $scope.scripts = scripts.data;

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.save = function(data) {
            siteScriptsService.update(data).success(function() {

                $.notify('Сохранено!', 'success');

            }).error(function() {

                $.notify('Возникла ошибка при сохранении!', 'error');

            });
        };

    }])
    .controller('SitesPagesCreateController', ['$scope', 'sitePagesService', '$location', '$stateParams', function($scope, sitePagesService, $location, $stateParams) {

        $scope.formData = {};

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.siteId = $stateParams.siteId;

        $scope.createPage = function(pageData) {
            sitePagesService.create(pageData).success(function(data) {
                $.notify('Страница успешно создана!', 'success');
                $location.path('sites/' + $stateParams.siteId + '/pages');
            }).error(function(data) {
                $.notify('Произошла ошибка', 'error');
            });
        };
    }])

    .controller('SitesMenuController', ['$scope', '$document', function($scope, $document) {
        /* Контроллер для управления меню сайта */
        $document[0].title = 'Редактирование основного меню сайта';




    }])
    .controller('SitesHeaderController', ['$scope', '$document', 'header', 'siteHeaderService', function($scope, $document, header, siteHeaderService) {
        /* Контроллер для управления хедером сайта */
        $document[0].title = 'Редактирование хедера сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };
        $scope.header = {};
        $scope.header.body = header.data;

        $scope.save = function(data) {
            siteHeaderService.save(data).success(function() {
                $.notify('Сохранено!', 'success');
            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };



    }])
    .controller('SitesFooterController', ['$scope', '$document', 'footer', 'siteFooterService', function($scope, $document, footer, siteFooterService) {
        /* Контроллер для управления футером сайта */
        $document[0].title = 'Редактирование футера сайта';

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        $scope.footer = {};
        $scope.footer.body = footer.data;



        $scope.save = function(data) {
            siteFooterService.save(data).success(function() {
                $.notify('Сохранено!', 'success');
            }).error(function() {
                $.notify('Возникла ошибка при сохранении!', 'error');
            });
        };


    }]);