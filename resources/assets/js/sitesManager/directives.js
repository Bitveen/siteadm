/*
* Модуль с директивами менеджера сайтов
* */
angular.module('sitesManager.directives', [])
    .directive('vmWidgetText', function() {
        return {
            restrict: 'EA',
            scope: {
                title: '=vmTitle',
                class: '=vmClass',
                content: '=vmContent',
                save: '&vmSave'
            },
            templateUrl: '/views/widgets/textWidget.html',
            link: function(scope, elem, attrs) {


            }
        };
    })
    .directive('vmWidgetBanners', function() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: '/views/widgets/bannersWidget.html',
            controller: function($scope) {

            },
            link: function(scope, elem, attrs) {

            }
        };
    })
    .directive('vmTermsTree', function() {
        return {

            restrict: 'A',

            scope: {
                terms: '=vmTreeData'
            },

            link: function(scope, elem, attrs) {

                var data = scope.terms;

                var mainUl = angular.element('<ul>'); // корень
                var mainLi = angular.element('<li>');

                for (var i = 0; i < data.children.length; i++) {
                    // по детям
                    var ul = angular.element('<ul>');
                    var elementChildren = data.children[i];

                    for (var j = 0; j < elementChildren.length; j++) {
                            var li
                    }


                }



            }

        };
    });