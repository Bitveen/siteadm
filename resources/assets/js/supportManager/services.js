/*
* Модуль с сервисами для тех. поддержки
* */
angular.module('supportManager.services', [])
    .factory('supportService', function($http) {
        /* Сервис для управления заявками */
        var _saveColor = function(item, color) {
            switch(color) {
                case 'blue':
                    color = 'primary';
                    break;
                case 'orange':
                    color = 'secondary';
                    break;
                case 'default':
                    color = 'default';
                    break;
            }
            var data = {
                color: color
            };

            return $http({
                method: 'PUT',
                url: '/api/support/color/' + item.id,
                data: data
            })

        };

        var _restoreColors = function(item, color) {
            switch(color) {
                case 'primary':
                    item.primaryColor = true;
                    item.secondaryColor = false;
                    break;
                case 'secondary':
                    item.secondaryColor = true;
                    item.primaryColor = false;
                    break;
                case 'default':
                    item.secondaryColor = false;
                    item.primaryColor = false;
                    break;
            }
        };


        var _check = function(supportId) {
            return $http({
                url: '/api/support/' + supportId,
                data: {},
                method: 'PUT'
            });
        };

        var _create = function(supportData) {
            return $http.post('/api/support', supportData);
        };


        return {
            saveColor: _saveColor,
            restoreColors: _restoreColors,
            check: _check,
            create: _create
        };
    })
    .factory('supportFilesService', function($http) {
        /* Сервис по работе с файлами заявок */


        var _drop = function(file) {
            return $http.post('/api/files?type=support&action=drop&filePath=' + file.path, {});
        };

        return {
            drop: _drop

        };



    });