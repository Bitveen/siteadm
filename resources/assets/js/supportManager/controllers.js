/*
* Модуль с контроллерами для тех. поддержки
* */
angular.module('supportManager.controllers', [])
    .controller('SupportListController', ['$scope', '$document', 'supportService', 'supportList', '$http', function($scope, $document, supportService, supportList, $http) {
        /*
        * Контроллер для отображения и взаимодействия с заявками в тех. поддержку
        * supportItems - список(массив) заявок в тех. поддержку
        * */
        $document[0].title = 'Тех. поддержка';
        $scope.supportItems = supportList.data;


        /* Восстановление цвета заявок при смене стейтов(к примеру) */
        $scope.supportItems.forEach(function(item) {
            switch(item.color) {
                case 'primary':
                    item.primaryColor = true;
                    item.secondaryColor = false;
                    break;
                case 'secondary':
                    item.secondaryColor = true;
                    item.primaryColor = false;
                    break;
                case 'default':
                    item.secondaryColor = false;
                    item.primaryColor = false;
                    break;
            }
        });


        $scope.changeColor = function(item, color) {

            switch(color) {
                case 'blue':
                    color = 'primary';
                    break;
                case 'orange':
                    color = 'secondary';
                    break;
                case 'default':
                    color = 'default';
                    break;
            }


            var data = {
                color: color
            };

            $http({
                method: 'PUT',
                url: '/api/support/color/' + item.id,
                data: data
            }).success(function(data) {
                //присвоить класс элементу
                console.log(data);


                switch(color) {
                    case 'primary':
                        item.primaryColor = true;
                        item.secondaryColor = false;
                        break;
                    case 'secondary':
                        item.secondaryColor = true;
                        item.primaryColor = false;
                        break;
                    case 'default':
                        item.secondaryColor = false;
                        item.primaryColor = false;
                        break;
                }

            });
        };


    }])
    .controller('SupportViewController', ['$scope', '$document', 'supportService', '$stateParams', 'supportData', function($scope, $document, supportService, $stateParams, supportData) {
        /*
        * Контроллер для просмотра заявки
        * */
        $document[0].title = 'Просмотр заявки';

        $scope.support = supportData.data.supportData;
        $scope.support.files = supportData.data.supportFiles;
        var supportId = $stateParams.supportId;





        $scope.updateSupportItem = function() {

            supportService.check(supportId)
                .success(function() {
                    console.log('Заявка успешно обработана!');

                    $.notify('Заявка успешно обработана.', {
                        globalPosition: "top center",
                        className: "success"
                    });
                })
                .error(function() {
                    console.log('Ошибка при обработке заявки!');
                });
        };


    }])
    .controller('SupportCreateController', ['$scope', '$document', 'supportService', 'supportFilesService', 'Upload', function($scope, $document, supportService, supportFilesService, Upload) {
        /*
        * Контроллер для создания новой заявки
        * */
        $document[0].title = 'Создание заявки в тех. поддержку';

        $scope.support = {};
        $scope.support.files = [];

        $scope.sendSupportMessage = function() {

            if ($scope.supForm.$invalid) {
                return;
            }
            supportService.create($scope.support);
        };

        $scope.dropFile = function(file) {

            supportFilesService.drop(file)
                .success(function() {
                    console.log('Файл успешно удаален!');

                    var index = $scope.support.files.indexOf(file);
                    $scope.support.files.splice(index, 1);

                })
                .error(function() {
                    console.log('Ошибка при удалении файла!');
                });

        };

        $scope.upload = function(files) {

            if (files && files.length) {

                for (var i = 0; i < files.length; i++) {

                    var file = files[i];

                    Upload
                        .upload({
                            url: '/api/files?type=support&action=upload',
                            file: file
                        })
                        .success(function(data, status, headers, config) {
                            config.file.path = data.path;
                            config.file.realName = config.file.name;
                            $scope.showUploadBar = false;
                            $scope.support.files.push(config.file);
                        })
                        .error(function() {
                            console.log('Ошибка при загрузке файла!');
                        })
                        .xhr(function(xhr) {
                            xhr.addEventListener('loadstart', function(event) {
                                $scope.showUploadBar = true;
                            }, false);
                        });

                }
                $scope.showUploadBar = false;

            }

        };


    }]);
