/*
* Модуль для работы с заявками в тех. поддержку
* */
angular.module('supportManager', ['supportManager.controllers', 'supportManager.services', 'supportManager.directives', 'supportManager.filters'])
    .config(function($stateProvider) {

        /* Список заявок */
        $stateProvider.state('supportList', {
            url: '/support',
            templateUrl: '/templates/supportManager/supportList.html',
            resolve: {
                supportList: function($http) {
                    return $http.get('/api/support');
                }
            },
            controller: 'SupportListController'
        });

        /* Просмотр одной заявки */
        $stateProvider.state('supportView', {
            url: '/support/{supportId:[0-9]{1,}}',
            templateUrl: '/templates/supportManager/supportView.html',
            resolve: {
                supportData: function($http, $stateParams) {
                    return $http.get('/api/support/' + $stateParams.supportId);
                }
            },
            controller: 'SupportViewController'
        });


        /* Создание заявки */
        $stateProvider.state('supportCreate', {
            url: '/support/create',
            templateUrl: '/templates/supportManager/supportCreate.html',
            controller: 'SupportCreateController'
        });


    });