/*
* Модуль с фильтрами для тех. поддержки
* */
angular.module('supportManager.filters', [])
    .filter('parseSupportStatus', function() {
        return function(rawStatus) {
            var status;

            switch (rawStatus) {
                case 'processed':
                    status = 'Обработано';
                    break;
                case 'send':
                    status = 'В обработке';
                    break;
            }

            return status;


        };
    });