angular.module('app.filters', [])
    .filter('parseDate', function() {
        return function(rawDate) {
            var date = rawDate.split(' ')[0].split('-');
            var day = date[2];
            var month = date[1];
            var year = date[0];

            return day + '.' + month + '.' + year;
        };
});