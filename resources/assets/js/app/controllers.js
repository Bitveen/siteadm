/*
* Модуль с общими контроллерами приложения
* */
angular.module('app.controllers', [])
    .controller('PageController', ['$scope', '$auth', '$state', '$rootScope', '$location', 'userHandler', function($scope, $auth, $state, $rootScope, $location, userHandler) {
        NProgress.configure({ showSpinner: false });

        $scope.logout = function() {

            $auth.logout().then(function() {
                localStorage.removeItem('user');
                $rootScope.authenticated = false;
                $rootScope.currentUser = {};
                userHandler.clear();
                $location.path('/');
            });

        };

    }]);