angular.module('app', [
    'ui.router',
    'auth',
    'publicationsManager',
    'usersManager',
    'supportManager',
    'sitesManager',
    'app.controllers',
    'app.filters'
])
    .config(function($provide, $httpProvider, $authProvider, $stateProvider, userHandlerProvider, $urlRouterProvider) {

        userHandlerProvider.load();

        /*
        * Автоматическая смена состояния, когда возвращается авторизованный пользователь
        * */
        if (userHandlerProvider.getCurrentUser()) {
            switch (userHandlerProvider.getCurrentUser().role) {
                case 'admin':
                    $urlRouterProvider.when('/', '/publications');
                    break;
                case 'user':
                case 'user-moderator':
                    $urlRouterProvider.when('/', '/publications/create');
                    break;
                case 'moderator':
                    $urlRouterProvider.when('/', '/publications');
                    break;
            }


        } else {

        }



        /* Для CORS на всякий случай */
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];


        function redirectWhenLoggedOut($q, $injector, $rootScope, $location) {


            return {

                'responseError': function(rejection) {

                    //var $location = $injector.get('$location');

                    var rejectReasons = [
                        'token_expired',
                        'token_invalid',
                        'token_absent',
                        'token_not_provided'
                    ];




                    angular.forEach(rejectReasons, function(value, key) {



                        if (rejection.data.error === value) {
                            localStorage.removeItem('user');
                            $rootScope.alertText = "Время сессии окончено. Перезайдите.";
                            $rootScope.showAlertBlock = true;
                            $rootScope.authenticated = false;
                            $rootScope.currentUser = null;
                            $location.url('/login');
                        }


                    });




                    return $q.reject(rejection);
                }


            };
        }



        $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);


        $httpProvider.interceptors.push('redirectWhenLoggedOut');

    })
    .run(function($rootScope, $location, userHandler) {

        $rootScope.$on('$stateChangeStart', function(event) {
            if (userHandler.get()) {
                $rootScope.authenticated = true;
            }
            NProgress.start();
        });

        
        $rootScope.$on('$stateChangeSuccess', function() {
            NProgress.done();
        });

        moment.locale('ru');

        if ($location.path() == "") {
            $location.path('/');
        }
    });



