angular.module('auth.services', []);
angular.module('auth.services').provider('userHandler', function() {
    var user = {};

    return {
        load: function() {
            var localUser = JSON.parse(localStorage.getItem('user'));
            if (localUser) {
                user = localUser;
            }
        },
        getCurrentUser: function() {
            return user;
        },
        $get: function() {
            return {
                get: function() {
                    return user;
                },
                clear: function() {
                    user = {};
                }
            };
        }
    };


});