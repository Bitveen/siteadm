/*
* Модуль отвечает за авторизацию пользователей
* */
angular.module('auth', ['auth.controllers', 'satellizer', 'auth.services', 'auth.directives'])
    .config(function($authProvider, $stateProvider, $urlRouterProvider) {
        $authProvider.loginUrl = '/api/authenticate';
    });