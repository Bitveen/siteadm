/*
* Модуль с контроллерами для авторизации
* */
angular.module('auth.controllers', [])
    .controller('AuthController', ['$scope', '$state', '$document', '$auth', '$rootScope', '$http', 'userHandler', function($scope, $state, $document, $auth, $rootScope, $http, userHandler) {
        /*
        * Основной контроллер, отвечающий за авторизацию
        * */
        $document[0].title = 'Вход';
        $scope.user = {};

        $rootScope.showAlertBlock = false;

        $rootScope.alertText = "";

        $scope.isRequestSend = false;

        $scope.authenticate = function() {
            $scope.isRequestSend = true;
            var credentials = {
                login: $scope.user.login,
                password: $scope.user.password
            };
            $auth.login(credentials).then(function() {
                return $http.get('/api/authenticate/user');
            }, function() {
                $scope.isRequestSend = false;
                $rootScope.showAlertBlock = true;
                $scope.user.password = "";
                $rootScope.alertText = "Неправильно указан логин или пароль.";
            }).then(function(response) {
                if (response) {
                    $scope.isRequestSend = false;
                    var user = JSON.stringify(response.data.user);
                    localStorage.setItem('user', user);
                    $rootScope.authenticated = true;
                    $rootScope.currentUser = response.data.user;
                    $state.go('publications');
                }
            });
        };

        $scope.closeAlert = function() {
            $rootScope.showAlertBlock = false;
        };


    }]);