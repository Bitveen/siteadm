/*
* Модуль по управлению ппользователями
* */
angular.module('usersManager', ['usersManager.controllers', 'usersManager.directives', 'usersManager.services', 'usersManager.filters'])
    .config(function($stateProvider) {
        /* Список пользователей */
        $stateProvider.state('usersList', {
            url: '/users',
            templateUrl: '/templates/usersManager/usersList.html',
            resolve: {
                usersList: function($http) {
                    return $http.get('/api/users');
                }
            },
            controller: 'UsersListController'
        });

        /* Форма создания пользователя */
        $stateProvider.state('usersCreate', {
            url: '/users/create',
            templateUrl: '/templates/usersManager/usersCreate.html',
            controller: 'UsersCreateController'
        });

        /* Просмотр и редактирование информации о пользователе */
        $stateProvider.state('usersView', {
            url: '/users/{userId:[0-9]{1,}}',
            templateUrl: '/templates/usersManager/usersView.html',
            controller: 'UsersViewController'
        });


    });
