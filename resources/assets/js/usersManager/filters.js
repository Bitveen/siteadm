/*
* Модуль с фильтрами для пользователей
* */
angular.module('usersManager.filters', [])
    .filter('parseRole', function() {
        /*
        * Фильтр для human-readable отображения названия ролей пользователей
        * */
        return function(rawRole) {
            var role;
            switch (rawRole) {
                case 'user':
                    role = 'Пользователь';
                    break;
                case 'moderator':
                    role = 'Модератор';
                    break;
                case 'user-moderator':
                    role = 'Пользователь-модератор';
                    break;
                default:
                    role = 'Unknown';
            }
            return role;
        };
    });