/*
* Модуль с контроллерами для пользователей
* */
angular.module('usersManager.controllers', [])
    .controller('UsersListController', ['$scope', '$document', 'usersList', function($scope, $document, usersList) {
        /*
        * Контроллер для списка пользователей
        * usersData - массив с пользователями
        * */
        $document[0].title = 'Пользователи';
        $scope.users = usersList.data;



    }])
    .controller('UsersCreateController', ['$scope', '$document', 'usersService', function($scope, $document, usersService) {
        /*
        * Контроллер для создания нового пользователя
        * */
        $document[0].title = 'Создание нового пользователя';

        $scope.createUser = function(user) {
            usersService.create(user)
                .success(function() {
                    console.log('Пользователь успешно создан!');
                })
                .error(function() {
                    console.log('Ошибка при создании пользователя!');
                });
        };

    }])
    .controller('UsersViewController', ['$scope', '$document', function($scope, $document) {
        /*
        * Контроллер для просмотра информации о пользователе и редактировния
        * */
        $document[0].title = 'Просмотр профиля пользователя';




    }]);