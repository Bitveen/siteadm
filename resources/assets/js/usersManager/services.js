/*
* Модуль с сервисами для пользователей
* */
angular.module('usersManager.services', [])
    .factory('usersService', ['$http', function($http) {
        /*
        * Сервис для управления пользователями(CRUD)
        * */
        var _create = function(user) {
            return $http.post('/api/users', user);
        };

        return {
            create: _create
        };

    }]);