/*
* Модуль с сервисами для публикаций
* */
angular.module('publicationsManager.services', [])
    .factory('filesHandler', function() {

        var _validateFile = function(file) {

            var availableFileTypes = [
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.ms-excel',
                'image/png',
                'image/jpeg',
                'image/gif',
                'application/pdf'
            ];

            for (var i = 0; i < availableFileTypes.length; i++) {
                if (file.type === availableFileTypes[i]) {
                    return true;
                }
            }

            return false;
        };
        var _isMainFileSet = function(files) {
            for (var i = 0; i < files.length; i++) {
                if (files[i].isMain === true) {
                    return true;
                }
            }
            return false;
        };

        var _makeMimeTypeByExtension = function(fileName) {
            var extension = fileName.slice(fileName.lastIndexOf(".") + 1);
            var type = "";
            switch (extension) {
                case "doc":
                    type = "application/msword";
                    break;
                case "docx":
                    type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
            }
            return type;
        };

        var _drop = function(file) {
            return $http.post('/api/files?type=publications&action=drop&filePath=' + file.path, {});
        };



        return {
            validateFile: _validateFile,
            isMainFileSet: _isMainFileSet,
            makeMimeTypeByExtension: _makeMimeTypeByExtension,
            drop: _drop
        };


    })
    .factory('publicationsService', function($http) {

        var _create = function(publicationData) {
            return $http.post('/api/publications', publicationData);
        };

        var _delete = function(publicationId) {
            return $http.post('/api/publications/' + publicationId, {});
        };

        var _publish = function(publicationData) {
            return $http({
                method: 'PUT',
                url: '/api/publications/' + publicationData.id,
                data: publicationData
            });
        };

        return {
            create: _create,
            delete: _delete,
            publish: _publish
        };
    })
    .factory('termsService', function($http) {


        var _get = function(siteId, value) {
            return $http.get('/api/terms?siteId=' + siteId + '&search=' + value);
        };

        return {
            get: _get
        };

    });