/*
* Модуль по управлению публикациями
* */
angular.module('publicationsManager', ['publicationsManager.controllers', 'publicationsManager.directives', 'publicationsManager.services', 'publicationsManager.filters'])
    .config(function($stateProvider) {

        /* Список всех публикаций */
        $stateProvider.state('publicationsList', {
            url: '/publications',
            templateUrl: '/templates/publicationsManager/publicationsList.html',
            resolve: {
                publicationsList: function($http) {
                    return $http.get('/api/publications');
                }
            },
            controller: 'PublicationsListController'
        });

        /* Просмотр одной публикации, при этом можно редактировать ее при определенных обстоятельствах */
        $stateProvider.state('publicationsView', {
            url: '/publications/{publicationId:int}',
            templateUrl: '/templates/publicationsManager/publicationsView.html',
            resolve: {
                publicationData: function($http, $stateParams) {
                    return $http.get('/api/publications/' + $stateParams.publicationId);
                }
            },
            controller: 'PublicationsViewController'
        });

        /* Создание новой публикации */
        $stateProvider.state('publicationsCreate', {
            url: '/publications/create',
            templateUrl: '/templates/publicationsManager/publicationsCreate.html',
            controller: 'PublicationsCreateController'
        });

        /* Архив с публикациями(список) */
        $stateProvider.state('publicationsArchive', {
            url: '/publications/archive',
            templateUrl: '/templates/publicationsManager/publicationsArchive.html',
            resolve: {},
            controller: 'PublicationsArchiveController'
        });
    });