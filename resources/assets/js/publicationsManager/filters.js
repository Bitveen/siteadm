/*
* Модуль с фильтрами для публикаций
* */
angular.module('publicationsManager.filters', [])
    .filter('parsePubStatus', function() {
        return function(rawStatus) {
            var status;
            switch (rawStatus) {
                case 'moderation':
                    status = 'В обработке';
                    break;
                case 'published':
                    status = 'Опубликовано';
                    break;
                default:
                    status = 'Unknown';
            }
            return status;
        };
    });