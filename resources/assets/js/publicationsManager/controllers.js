/*
* Модуль с контроллерами для публикаций
* */
angular.module('publicationsManager.controllers', [])
    .controller('PublicationsListController', ['$scope', '$document', 'publicationsList', function($scope, $document, publicationsList) {
        /*
        * Контроллер для списка публикаций, разные отображения для разных ролей
        * */
        $document[0].title = 'Публикации';
        $scope.publications = publicationsList.data;



    }])
    .controller('PublicationsViewController', ['$scope', '$document', 'termsService', 'publicationsService', 'publicationData', function($scope, $document, termsService, publicationsService, publicationData) {
        /*
        * Контроллер для просмотра публикации
        * */
        $document[0].title = 'Просмотр публикации';



        $scope.publication = publicationData.data.publication;

        $scope.codeOptions = {
            lineNumbers: true,
            mode: "text/css"
        };

        var pubDateElement =  $('#pub-date');

        pubDateElement.datepicker({
            language: "ru"
        });


        $scope.showTerms = false;


        $scope.publication.pubDate = moment($scope.publication.pubDate, "YYYY-MM-DD").format('L');


        pubDateElement.on('blur', function(event) {

            if (!$scope.publication.pubDate || !event.target.value) {
                event.target.value = $scope.publication.pubDate;
            } else {
                $scope.publication.pubDate =  event.target.value;
            }


        });


        // для файлов
        $scope.publication.files = publicationData.data.files;


        for (var i = 1; i < publicationData.data.length - 1; i++) {
             $scope.publication.files.push({
                name: publicationData.data[i].name,
                path: publicationData.data[i].path,
                isMain: publicationData.data[i].isMain
             });
        }

        $scope.publication.site = {
            id: publicationData.data.sites[0].id,
            title: publicationData.data.sites[0].title
        };


        $scope.sendPublication = function() {

            if ($scope.pubForm.$valid) {

                publicationsService.publish($scope.publication).success(function() {

                    $.notify('Опубликовано.', {
                        globalPosition: "top center",
                        className: "success"
                    });

                    $location.path('admin/publications/posted');

                }).error(function() {


                });

            }


        };

        $scope.publication.receivedTerms = [];

        $scope.publication.chosenTerms = [];

        $scope.isSendTermsRequest = false;


        $scope.$watch('publication.termSearchItem', function(newValue) {


            if (typeof newValue == "string" && newValue.length > 2) {

                termsService.get($scope.publication.site.id, newValue).success(function(data) {

                    $scope.isSendTermsRequest = true;
                    $scope.publication.receivedTerms = [];

                    var chosenLength = $scope.publication.chosenTerms.length;

                    if (chosenLength == 0) {
                        $scope.publication.receivedTerms = data;
                    } else {

                        var receivedItemsLength = data.length;

                        // цикл по полученным элементам
                        for (var j = 0; j < receivedItemsLength; j++) {
                            var id = data[j].id;
                            var itemFound = false;
                            //цикл для проверки, есть ли такой элемент уже в выбранных
                            for (var i = 0; i < chosenLength; i++) {
                                if ($scope.publication.chosenTerms[i].id == id) {
                                    itemFound = true;
                                    break;
                                }
                            }
                            if (!itemFound) {
                                $scope.publication.receivedTerms.push(data[j]);
                            }
                            itemFound = false;
                        }
                    }

                }).error(function() {



                });


            } else {
                $scope.isSendTermsRequest = false;
                $scope.publication.receivedTerms = [];
            }

        });


        $scope.choseTerm = function(term) {
            $scope.publication.chosenTerms.push(term);
            var index = $scope.publication.receivedTerms.indexOf(term);
            $scope.publication.receivedTerms.splice(index, 1);
        };

        $scope.dropTerm = function(term) {
            $scope.publication.receivedTerms.push(term);
            var index = $scope.publication.chosenTerms.indexOf(term);
            $scope.publication.chosenTerms.splice(index, 1);
        };

        $scope.deletePublication = function(publicationId) {

            publicationsService.delete(publicationId).success(function() {

                $.notify('Публикация успешно удалена.', {
                    globalPosition: "top center",
                    className: "success"
                });

            }).error(function() {

                $.notify('Возникла ошибка при удалении. Повторите запрос позже.', {
                    globalPosition: "top center",
                    className: "error"
                });

            });

        };



    }])
    .controller('PublicationsCreateController', ['$scope', '$document', 'filesHandler', 'publicationsService', 'Upload', function($scope, $document, filesHandler, publicationsService, Upload) {
        /*
        * Контроллер для создания публикации
        * */
        $document[0].title = 'Создание новой публикации';


        $scope.publication = {};
        $scope.publication.files = [];

        var pubDateElement = $('#pub-date');

        pubDateElement.datepicker({
            language: "ru"
        });


        $scope.publication.pubDate = moment().format("L");

        $scope.showUploadBar = false;



        /*
         * Для календаря
         * */
        pubDateElement.on('blur', function(event) {

            if (!$scope.publication.pubDate || !event.target.value) {
                event.target.value = $scope.publication.pubDate;
            } else {
                $scope.publication.pubDate =  event.target.value;
            }

        });

        $scope.validateFile = function(file) {
            return filesHandler.validateFile(file);
        };

        $scope.validateMainFile = function(file) {
            return file.mimeType === 'application/msword' || file.mimeType === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        };

        $scope.dropFile = function(file) {

            filesHandler.drop(file).success(function() {
                console.log('Файл успешно удален!');
                var index = $scope.publication.files.indexOf(file);
                $scope.publication.files.splice(index, 1);

                if (file.isMain) {
                    $scope.publication.title = "";
                }

            }).error(function() {
                console.log('Ошибка при удалении файла!');
            });

        };

        $scope.changeMainFile = function(file) {

            for (var i = 0; i < $scope.publication.files.length; i++) {
                if ($scope.publication.files[i].isMain) {
                    $scope.publication.files[i].isMain = false;
                    break;
                }
            }

            var title = file.name;
            $scope.publication.title = title.substr(0, title.lastIndexOf('.'));
            file.isMain = true;

        };

        $scope.createPublication = function() {

            if ($scope.pubForm.$valid) {

                if ($scope.publication.files.length == 0) {
                    return;
                }

                /*if (!isMainFileSet($scope.publication.files)) {
                 return;
                 }*/

                publicationsService.create($scope.publication).success(function() {

                    console.log('Публикация успешно создана');

                }).error(function() {

                    console.log('Ошибка при создании публикации!');

                });

            }

        };

        $scope.upload = function(files) {

            if (files && files.length) {

                for (var i = 0; i < files.length; i++) {

                    var file = files[i];

                    Upload.upload({

                        url: '/api/files?type=publication&action=upload',
                        file: file

                    }).success(function(data, status, headers, config) {

                        config.file.path = data.path;
                        config.file.mimeType = config.file.type || filesHandler.makeMimeTypeByExtension(config.file.name);
                        config.file.realName = config.file.name;
                        config.file.isMain = false;
                        $scope.showUploadBar = false;
                        $scope.publication.files.push(config.file);

                        console.log($scope.publication.files);

                    }).xhr(function(xhr) {

                        xhr.addEventListener('loadstart', function(event) {

                            $scope.showUploadBar = true;

                        }, false);

                    });
                    $scope.showUploadBar = false;
                }
            }

        };



    }])
    .controller('PublicationsArchiveController', ['$scope', '$document', function($scope, $document) {
        /*
        * Контроллер для отображения списка архивных публикаций
        * */



    }]);