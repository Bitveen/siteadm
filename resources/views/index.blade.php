<!doctype html>
<html lang="ru" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <title>siteadm.pro</title>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/nprogress.css">
    <link rel="stylesheet" href="/css/codemirror.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="/css/ngDialog.css">
    <link rel="stylesheet" href="/css/ngDialog-theme-default.css">

</head>
<body  ng-controller="PageController">

    <div class="wrapper"  ng-controller="AuthController" ng-cloak>
        <div ng-if="authenticated">
            <!-- подключить определенный базовый шаблон на основе роли авторизованного пользователя -->
            <div ng-include="'/templates/admin.html'"></div>
        </div>
        <div ng-if="!authenticated">
            <!-- показать форму авторизации -->
            <div ng-include="'/templates/login.html'"></div>
        </div>
    </div>




    <script src="js/vendor/angular.js"></script>
    <script src="js/vendor/angular-locale_ru-ru.js"></script>
    <script src="js/vendor/angular-ui-router.js"></script>
    <script src="js/vendor/satellizer.js"></script>
    <script src="js/vendor/nprogress.js"></script>
    <script src="js/vendor/jquery-2.1.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/notify.min.js"></script>
    <script src="js/vendor/moment.js"></script>
    <script src="js/vendor/ng-file-upload.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.min.js"></script>
    <script src="js/vendor/bootstrap-datepicker.ru.min.js"></script>
    <script src="js/vendor/codemirror.js"></script>
    <script src="js/vendor/css.js"></script>
    <script src="js/vendor/ui-codemirror.js"></script>
    <script src="js/vendor/ngDialog.js"></script>
    <script src="js/siteAdmApp.js"></script>
</body>
</html>